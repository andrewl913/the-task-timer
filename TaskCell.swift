//
//  TaskCell.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 6/24/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class TaskCell: UITableViewCell {
  
  @IBOutlet weak var cellText: UILabel!
  var taskTextField : UITextField?
  var taskManager : PomoTaskManager?
  var finishedButton : FinishButton?
  
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    hideTextField();
  }
  
  init(style: UITableViewCellStyle, reuseIdentifier: String?, taskManager : PomoTaskManager) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    self.taskManager = taskManager;
    self.selectionStyle = UITableViewCellSelectionStyle.None
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    //fatalError("init(coder:) has not been implemented")
  }
  
  
  override func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  func hideTextField() {
    self.taskTextField?.hidden = true;
    self.taskTextField?.removeFromSuperview();
    // we are about to add text to the table view if it isn't empty
    if(self.taskTextField?.text != "") {
      showFinishButton();
    }
  }
  
  func showTextField() {
    taskTextField = UITextField(frame: self.frame)
    taskTextField?.returnKeyType = UIReturnKeyType.Done
    taskTextField!.placeholder = "click add button to add a task..."
    taskTextField!.delegate = self.taskManager
    
    if(taskManager == nil) {
      print("ERROR: task manager doesn't exist\n", terminator: "")
    }
    self.addSubview(taskTextField!)
  }
  
  
 private func showFinishButton() {
    let contentWidth = self.frame.width
    let contentHeight = self.frame.height
    let sizeOfButton = CGSizeMake(50,  50)
    let buttonFrame = CGRectMake( contentWidth / 1.17 - sizeOfButton.width ,( contentHeight - sizeOfButton.height)/2   , sizeOfButton.width, sizeOfButton.height)
    
    
    self.finishedButton = FinishButton(frame: buttonFrame)
    self.finishedButton?.setTitle("◉", forState: .Normal)
    self.finishedButton?.titleLabel?.font = UIFont.systemFontOfSize(30)
    self.finishedButton?.setTitleColor(PomoUtils.tomatoRedColor, forState: .Normal)
    self.finishedButton?.addTarget(self.taskManager, action: "finishedButtonClicked:", forControlEvents: UIControlEvents.TouchUpInside)
    self.contentView.addSubview(self.finishedButton!)
    self.contentView.bringSubviewToFront(finishedButton!)
 
    setConstraintsForCell()
  
  }
  
 private func setConstraintsForCell() {
    
    for view in self.contentView.subviews {
    
      if(!(view is UILabel)) {
        
      }
    }
    
    let views = Dictionary(dictionaryLiteral: ("finishButton", finishedButton!), ("taskLabel", self.textLabel!));
    
    let leftButtonConstraintToSuperView = NSLayoutConstraint.constraintsWithVisualFormat("H:[finishButton]-4.0-|", options: [], metrics: nil, views: views)
    let leftButtonVertConstraint = NSLayoutConstraint.constraintsWithVisualFormat("V:[finishButton]", options: [], metrics: nil, views: views)
    
  
    
    
    
    self.contentView.addConstraints(leftButtonVertConstraint)
    self.contentView.addConstraints(leftButtonConstraintToSuperView)
    self.contentView.updateConstraints()
  }
  
  
  }

//
//  PomoUtility.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 6/24/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class PomoEvent {
  static let StartButtonClicked : Selector = "startButtonClicked:"
  static let StopButtonClicked : Selector = "stopButtonClicked:"
}

class PomoControllerUtils: NSObject {
  
  class func timerResetHelper(timer : PomoTimer, controller : MainViewController) {
    timer.reset();
    //controller.pushButtonsOffScreen()
    controller.addTomatoesToTimer(timer);
    timer.tomatoAnimator?.resetTomatoes()
    controller.startButton.resetButton()
    controller.startButton.resetStrokePosition()
    
    controller.startButton.removeTarget(controller, action: PomoEvent.StopButtonClicked, forControlEvents: .TouchUpInside)
    controller.startButton.addTarget(controller, action: PomoEvent.StartButtonClicked, forControlEvents: .TouchUpInside)
    
    // reset the tomato to gray (unfilled)
  }

  //since we are using the storyboard to layout our content with appropriate constraints, we must remove set all views back ground color to clear color.
  class func cleanUpBackgroundColors(viewController : UIViewController) {
    for view in viewController.view.subviews {
      let viewImage = view 
      viewImage.backgroundColor = UIColor.clearColor()
    }
  }
}

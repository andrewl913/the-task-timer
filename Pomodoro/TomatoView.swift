//
//  tomatoView.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 6/29/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class TomatoView: UIView {
  
  var pomoIncrement = false {
    didSet {
      self.setNeedsDisplay()
    }
  }
  
  override func drawRect(rect: CGRect) {
    self.backgroundColor = UIColor.clearColor();
    PomoUtils.drawTomatoIcon(frame: rect, pomoIncremented: pomoIncrement)
    
  }

}

//
//  Task.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 7/7/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//
import UIKit
import Parse
import Foundation
class PomoTask : NSObject , NSCoding {
    
    let taskText : String
    var isFinished : Bool
    var color : UIColor
    
   
    
    init(taskText : String, isFinished : Bool, color : UIColor) {
       
        self.taskText = taskText;
        self.isFinished = isFinished;
        self.color = color;
        super.init();
    
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
     self.taskText =   aDecoder.decodeObjectForKey("taskText")! as! String
     self.isFinished = aDecoder.decodeObjectForKey("isFinished")! as! Bool
     self.color = aDecoder.decodeObjectForKey("color")! as! UIColor
    }
    
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.taskText, forKey: "taskText")
        aCoder.encodeObject(self.isFinished, forKey: "isFinished")
        aCoder.encodeObject(self.color, forKey: "color")
    }
    
    func save() {
        let archivedObject = NSKeyedArchiver.archivedDataWithRootObject(self)
        NSUserDefaults.standardUserDefaults().setObject(archivedObject, forKey: "pomoTask")
    }
    
    func clear() {
        NSUserDefaults.standardUserDefaults().removeObjectForKey("pomoTask")
    }
    
    class func loadSaved() -> PomoTask? {
        if let archivedObject = NSUserDefaults.standardUserDefaults().objectForKey("pomoTask") as? NSData {
            return NSKeyedUnarchiver.unarchiveObjectWithData(archivedObject) as? PomoTask
        }
        return nil;
    }
}
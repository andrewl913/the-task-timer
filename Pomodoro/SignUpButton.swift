//
//  SignUpButton.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 7/2/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit
import Parse

class SignUpButton: UIButton {
    
    var signInTextLabel : UILabel?
    var startButton : StartButton?
    
  
    
    var loginPressed : Bool = false {
        didSet {
            
            setNeedsDisplay()
        }
    }
    var showStats : Bool =  false {
        didSet {
            // setNeedsDisplay()
            
            let user = PFUser.currentUser();
            
            if (user != nil) {
                showStats = true;
                startButton?.showStats = true;
                setNeedsDisplay()
            }
        }
    }
    
    
    
    
    var loginTextOffset : CGFloat = 40 {
        didSet {
            setNeedsDisplay()
        }
    }
    var personAngle : CGFloat  = 0 {
        didSet {
            setNeedsDisplay();
        }
    }
    
    
    var personOffset : CGFloat  = 0 {
        didSet {
            setNeedsDisplay();
        }
    }
    
    var deltaTimer : NSTimer?
    var delta : CGFloat = 0
    
    
    var distanceAway : CGFloat =  80;
    var loginCenterPosition : CGFloat?
    var centerPosition : CGFloat?
    var centerYPosition : CGFloat  {
        get {
            return self.bounds.height/2 - 20;
        }
    }
    
    
    var centerIsSet = false;
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
         
    }
    
    
    
    
    
    override func drawRect(rect: CGRect) {
        if(!centerIsSet){
            distanceAway = 100
            centerPosition = self.bounds.width/2 - 14;
            loginCenterPosition = centerPosition! - 25 + 14;
            personOffset = centerPosition!      //center the person icon
            loginTextOffset = loginCenterPosition! //- distanceAway //push this icon off the screen
            centerIsSet = true
            
        }
        
        
        
        
    PomoUtils.drawSignUpButton(frame: rect, personOffset: personOffset, loginTextPosition: loginTextOffset, statsPosition: loginTextOffset, showLog: loginPressed, showStat: showStats, yPosOfPerson: centerYPosition, logYPosition: centerYPosition + 20 - 12.65/2, statYPosition: centerYPosition + 20 - 12.65/2)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        
        invalidateTimers()
        animatePersonOffScreen()
      
        //self.showStats = false     //default to false.. if a user is signed it, this will call the didSet and update if need to the stats label
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        invalidateTimers()
        animatePersonOnScreen()
        self.startButton?.showStats = false
    }
    
    
    func animatePersonOffScreen() {
        deltaTimer = NSTimer.scheduledTimerWithTimeInterval(1/80, target: self, selector: "animatePosition", userInfo: nil, repeats: true)
    }
    
    func animatePersonOnScreen() {
        
        deltaTimer = NSTimer.scheduledTimerWithTimeInterval(1/80, target: self, selector: "animatePositionBack", userInfo: nil, repeats: true)
        
    }
    
    func invalidateTimers() {
        if let timer = deltaTimer {
            timer.invalidate()
        }
    }
    
    func animatePosition() {
        
        showStats = false
        if (showStats) {
            loginPressed = false
        } else {
            loginPressed = true;
        }
        personOffset = delta + centerPosition!
        loginTextOffset = delta + centerPosition! - distanceAway
        if personOffset >= loginCenterPosition! + distanceAway - 1 {
            self.enabled = true;
            delta = 0;
            deltaTimer?.invalidate()
            
            return
        }
        self.enabled = false
        
        delta+=4
    }
    
    func animatePositionBack() {
        personOffset = delta + centerPosition! + distanceAway
        loginTextOffset = delta
        if( personOffset == centerPosition!) {
            self.enabled = true
            delta = 0;
            deltaTimer?.invalidate()
            return;
        }
        self.enabled = false
        delta-=4;
    }
    
    
    
    
}



//
//  StatsTableViewController.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 7/8/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit
import Parse

class StatsTableViewController: UITableViewController {
    
    var pomoSession : PomoSession?
    var timelineViewManager : TimelineViewManager?
    var timelineTableViewController : TimelineTableViewController?
    var timelineHolderViewController : TimelineHolderViewController?
    
    @IBOutlet weak var currentStopCountLabel: UILabel!
    @IBOutlet weak var currentTaskCompletedCountLabel: UILabel!
    @IBOutlet weak var currentNumberOfTomatoesCountLabel: UILabel!
    @IBOutlet weak var currentOverallTimeCountLabel: UILabel!
    
    @IBOutlet weak var lastStopCountLabel: UILabel!
    @IBOutlet weak var lastTaskCompletedCountLabel: UILabel!
    @IBOutlet weak var lastNumberOfTomatoesCountLabel: UILabel!
    @IBOutlet weak var lastOverallTimeCountLabel: UILabel!
    
    @IBOutlet weak var longestSessiontimeCountLabel: UILabel!
    @IBOutlet weak var mostTaskCompleteLabel: UILabel!
    
    //*** CELL REFERENCES
    
    @IBOutlet weak var numStopsCell: UITableViewCell!
    @IBOutlet weak var tasksCompletedCell: UITableViewCell!
    @IBOutlet weak var numTomatoesCell: UITableViewCell!
    @IBOutlet weak var overallTimeCell: UITableViewCell!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        instantiateDetailedViewController()
        assignDelegatesToTimelineViewManager()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.tableView.alpha = 0
  
    }
    
    override func viewDidAppear(animated: Bool) {
        
        animateTableViewAndCells()
        updateStats()
    }
    
    
    @IBAction func stopTimelineButtonClicked(sender: TimelineButton) {
        
        
        if timelineViewManager!.isPopulating {
            // if the timeline is pulling data, don't allow any other buttons to trigger events
            
            return;
        }
        
        if !sender.isLoading {
            
            timelineViewManager?.graphRange = 400;
            timelineViewManager?.clearData() // empties out the buffer
            timelineViewManager!.populateTheTableView("stopCount")
            
            
            sender.isLoading = true;
        }
        
        print("stats count: \(timelineViewManager?.statValues.count  )", terminator: "")
        
    }
    
    @IBAction func taskTimelineButtonClicked(sender: TimelineButton) {
        
        if timelineViewManager!.isPopulating {
            return;
        }
        sender.isLoading = true;
        
        timelineViewManager?.clearData()
        
        timelineViewManager!.populateTheTableView("tasksFinishedCount")
    }
    
    @IBAction func tomatoesTimelineButtonClicked(sender: TimelineButton) {
        if timelineViewManager!.isPopulating {
            return;
        }
        timelineViewManager?.clearData()
        timelineViewManager?.populateTheTableView("numberOfTomatoes")
        sender.isLoading = true;
    }
    @IBAction func overallTimelineButtonClicked(sender: TimelineButton) {
        if timelineViewManager!.isPopulating {
            return;
        }
        
        timelineViewManager?.clearData()
        timelineViewManager?.graphRange = 0.2
        sender.isLoading = true;
        timelineViewManager?.populateTheTableView("overallSessionTime")
        
    }
    
    
    func assignDelegatesToTimelineViewManager() {
        if let timelineTableViewController = self.timelineTableViewController {
            
            timelineViewManager = TimelineViewManager(timelineTableViewController: timelineTableViewController, statsTableViewController : self, timelineHolderViewController: timelineHolderViewController!);
            
            timelineHolderViewController?.timelineViewManager = self.timelineViewManager
            timelineTableViewController.statsTableViewController = self;
        }
    }
    
    func updateStats() {
        let backgroundOp = NSOperationQueue()
        
        backgroundOp.addOperationWithBlock({
            
            self.updatePrevStats()
            self.updateLongestSession()
            self.updateMostFinishedTasks()
            self.compareWithLastSession()
            
        })
        
        self.updateCurrentStats()
    }
    
    func updateCurrentStats() {
        // this is the temp session needed to be stored until saved to parse
        if let session = pomoSession {
            
            currentStopCountLabel.text = String(session.stopCount);
            currentTaskCompletedCountLabel.text = String(session.tasksFinishedCount)
            currentNumberOfTomatoesCountLabel.text = String(session.numberOfTomatoes)
            currentOverallTimeCountLabel.text = secondsToTime(session.overallSessionTime)
        }
        
    }
    
    func updatePrevStats() {
        //TODO : Hook up with previous session stats
        if let user = PFUser.currentUser() {
            
            let query = PFQuery(className:"PomoSession");
            
            query.whereKey("user", equalTo: user)
            query.orderByDescending("createdAt")
            
            query.getFirstObjectInBackgroundWithBlock({object, error in
                if let lastSession = object {
                    
                    let lastSessionInt : AnyObject = lastSession["overallSessionTime"] == nil ? 0 : lastSession["overallSessionTime"]!
                    
                    self.lastStopCountLabel.text = String(stringInterpolationSegment: lastSession["stopCount"]!)
                    self.lastTaskCompletedCountLabel.text = String(stringInterpolationSegment: lastSession["tasksFinishedCount"]!);
                    self.lastNumberOfTomatoesCountLabel.text = String(stringInterpolationSegment: lastSession["numberOfTomatoes"]!)
                    self.lastOverallTimeCountLabel.text = self.secondsToTime(lastSessionInt as! Int)
                    
                }
                
            })
        }
        
    }
    
    func updateLongestSession() {
        if let user = PFUser.currentUser() {
            
            let query = PFQuery(className:"PomoSession");
            
            query.whereKey("user", equalTo: user)
            query.orderByDescending("overallSessionTime")
            query.getFirstObjectInBackgroundWithBlock{ object, error in
                
                if let longestSession = object {
                    let longestSessionInt : AnyObject = longestSession["overallSessionTime"] == nil ? 0 : longestSession["overallSessionTime"]!
                    
                    self.longestSessiontimeCountLabel.text = self.secondsToTime(longestSessionInt as! Int)
                }
                
            }
        }
    }
    
    
    func updateMostFinishedTasks() {
        
        if let user = PFUser.currentUser() {
            
            let query = PFQuery(className:"PomoSession");
            
            query.whereKey("user", equalTo: user)
            query.orderByDescending("tasksFinishedCount")
            query.getFirstObjectInBackgroundWithBlock{ object, error in
                
                if let mostTasksSession = object {
                    self.mostTaskCompleteLabel.text = String(stringInterpolationSegment: mostTasksSession["tasksFinishedCount"] ==  nil ?  0 : mostTasksSession["tasksFinishedCount"]!)
                }
                
            }
        }
        
        
    }
    
    
    
    
    func compareWithLastSession() {
        //take the integer values, and look at them
        //if the current item is large value
        //turn the text to green
        
        if let lastSession = pomoSession?.fetchDataForUser(PFUser.currentUser()!) {
            if let session = pomoSession {
                
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    self.currentStopCountLabel.textColor = session.stopCount < (lastSession["stopCount"] as! Int) ? PomoUtils.startGreenColor : PomoUtils.tomatoRedColor
                    self.currentTaskCompletedCountLabel.textColor = session.tasksFinishedCount  > (lastSession["tasksFinishedCount"] as! Int) ? PomoUtils.startGreenColor : PomoUtils.tomatoRedColor
                    self.currentNumberOfTomatoesCountLabel.textColor = session.numberOfTomatoes > (lastSession["numberOfTomatoes"] as! Int) ? PomoUtils.startGreenColor : PomoUtils.tomatoRedColor
                    self.currentOverallTimeCountLabel.textColor = session.overallSessionTime < (lastSession["overallSessionTime"] as! Int) ? PomoUtils.startGreenColor : PomoUtils.tomatoRedColor
                }
                
            }
        }
        
    }
    
    func secondsToTime(seconds: Int) -> String {
        let hour = seconds/3600
        let minute = (seconds % 3600)/60
        let second = (seconds % 3600) % 60
        
        if(minute < 10 && second < 10) {
            return "\(hour):0\(minute):0\(second)"; // second needs a zero prepended
        } else if minute < 10  {
            return  "\(hour):0\(minute):\(second)"; // minute needs a zero prepended
        } else if second < 10 {
            return  "\(hour):\(minute):0\(second)"; // both need zero prepended
        } else {
            return "\(hour):\(minute):\(second)"; // update the view every second
        }
        
    }
    
    
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        PomoAnimations.animateCell(cell, duration: 0.2)
    }
    
    func animateTableViewAndCells() {
        pushOfCells()
        animateCells()
        animateTableView()
    }
    
    func pushOfCells() {
        tableView.alpha = 0;
        for cell in tableView.visibleCells {
            cell.alpha = 0.0
        }
        
    }
    
    func animateCells() {
        let vertOffset = CGAffineTransformMakeTranslation(0, 30)
        
        var time = 0.0;
        for cell in tableView.visibleCells {
            cell.transform = vertOffset
            UIView.beginAnimations("Translation", context: nil)
            UIView.setAnimationDuration(0.5)
            
            UIView.setAnimationDelay(time)
            cell.transform = CGAffineTransformIdentity
            cell.alpha = 1
            
            UIView.commitAnimations()
            
            time += 0.2;
        }
        time = 0.5
    }
    
    
    func animateTableView() {
        UIView.animateWithDuration(0.2, animations: {
            self.tableView.alpha = 1;
            
        })
    }
    

    
    func instantiateDetailedViewController() {
        timelineTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("TimelineTableViewController") as? TimelineTableViewController
        timelineHolderViewController = self.storyboard?.instantiateViewControllerWithIdentifier("TimelineHolderViewController") as? TimelineHolderViewController
    }
    
    
    
    @IBOutlet weak var numStopsTimelineButton: TimelineButton!
    @IBOutlet weak var tasksCompletedTimelineButton: TimelineButton!
    @IBOutlet weak var numTomatoesTimelineButton: TimelineButton!
    @IBOutlet weak var overallTimeTimelineButton: TimelineButton!
    
    func resetCellButtonState() {
        changeButtonState(numStopsTimelineButton)
        changeButtonState(tasksCompletedTimelineButton)
        changeButtonState(numTomatoesTimelineButton)
        changeButtonState(overallTimeTimelineButton)
    }
    
    
    private func changeButtonState(button : TimelineButton) {
        button.isLoading = false
    }
    
    var counter = 0;
    
    var button : TimelineButton?
    
    
    func animateButtonWhileLoading(button : TimelineButton ,alternator: Int) {
        if button.isLoading {
            let size = alternator % 2 == 0 ? CGAffineTransformMakeScale(1.2, 1.2) : CGAffineTransformIdentity
            PomoAnimations.animationButtonSize(button, toSize: size, duration: 0.3);
        } else {
            PomoAnimations.animationButtonSize(button, toSize: CGAffineTransformIdentity, duration: 0.3);
        }
    }
    
    
    
    
    
    
    
    
    
}

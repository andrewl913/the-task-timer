//
//  StatsButtonDragger.swift
//  Task Timer
//
//  Created by Andrew Lincoln on 7/14/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class StatsButtonDragger: NSObject {
   
    var signInButton : SignUpButton?
    
    var panGestureRec = UIPanGestureRecognizer();
    
    init(signInButton : SignUpButton) {
        self.signInButton = signInButton
        super.init()
        
        panGestureRec.addTarget(self, action: "signInButtonPan:")
        self.signInButton?.addGestureRecognizer(panGestureRec)
    
    }
    
    
    func signInButtonPan(panGesture : UIPanGestureRecognizer) {
      // var translationPercent = 0.0
        self.signInButton!.center = panGesture.locationInView(signInButton!.superview!)
        let velocity = panGesture.velocityInView(self.signInButton!.superview!)
        let threshold = CGPoint(x: 4, y: 4)
        
      //  var mainViewControllerWidth = self.signInButton!.superview!.bounds.width
        
        
        if velocity.x > threshold.x {
            self.signInButton!.transform = CGAffineTransformIdentity
        }
        
        
        switch panGesture.state {
        case .Changed:
            print("hello", terminator: "");
        break;
        default:
            break;
        }
        
        
    }
    
    
}

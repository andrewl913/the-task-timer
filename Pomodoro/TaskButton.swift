//
//  TaskButton.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 6/29/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class TaskButton: UIButton {

  override func drawRect(rect: CGRect) {
    self.backgroundColor = UIColor.clearColor();
    PomoUtils.drawTasksButton(frame: rect)
  }
  
  
  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    super.touchesBegan(touches, withEvent: event)
    //angles in IOS are rotate in the -radian position
    PomoAnimations.rotateView(self, duration: 0.3, angle: CGFloat(-M_PI/6))
  }
  
  override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
    super.touchesEnded(touches, withEvent: event)
    PomoAnimations.resetToIdentity(self, duration: 0.3)
    
  }

  
  
}

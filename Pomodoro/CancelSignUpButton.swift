//
//  CancelSignUpButton.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 7/2/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class CancelSignUpButton : SignUpViewButton {

 
  
  override func drawRect(rect: CGRect) {
    PomoUtils.drawCancelOnViewControllerButton(frame: rect)
  }
  
  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    super.touchesBegan(touches, withEvent: event)
    self.superview?.bringSubviewToFront(self)
    animateViewSize()
    
   
    
    
  }
  
  
  override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
    super.touchesBegan(touches, withEvent: event)
    animationDone()
    signUpViewController?.cancelButtonPressed(self)
 
  }
  
  
  
  func animateViewSize() {
     PomoAnimations.animationViewSize(self, toSize: CGAffineTransformMakeScale(1.2, 1.2), duration: 0.3)
  
  }
  
  func animationDone() {
     PomoAnimations.animationViewSize(self, toSize: CGAffineTransformIdentity, duration: 0.3)
  }

}

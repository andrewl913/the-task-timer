//
//  TimelineButton.swift
//  Task Timer
//
//  Created by Andrew Lincoln on 7/12/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class TimelineButton: UIButton {
    var isLoading = false {
        didSet {
            self.setNeedsDisplay()
        }
    }
    override func drawRect(rect: CGRect) {
        
        PomoUtils.drawTimeLineButton(frame: rect, isLoading: isLoading);
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches , withEvent: event )
        PomoAnimations.animationCalendarButton(self, duration: 0.3, size: CGAffineTransformMakeScale(1.1, 1.1))
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event )
        
        PomoAnimations.animationCalendarButton(self, duration: 0.3, size: CGAffineTransformIdentity)
    }
}

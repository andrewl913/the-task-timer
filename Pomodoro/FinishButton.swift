//
//  FinishButton.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 6/25/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class FinishButton: UIButton {
    var isFinished = false {
        didSet {
            if(isFinished) {
                //println("changing finished color button" )
             PomoAnimations.animationButtonTextColor(self, toColor: PomoUtils.startGreenColor, duration: 0.5)
            } else {
                PomoAnimations.animationButtonTextColor(self, toColor: PomoUtils.tomatoRedColor, duration: 0.5)

            }
        }
    }

}

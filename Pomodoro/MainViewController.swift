//
//  MainViewController.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 5/17/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit
import Parse

class MainViewController: UIViewController, SetTimerDelegate  {
    
    var taskList : [PomoTask] = [PomoTask(taskText: "awesome", isFinished: false, color : PomoUtils.tomatoRedColor)]
   
    var timer : PomoTimer?
    var breakTimer : PomoTimer?
    var pomoSession : PomoSession?
    var modalVCManager : ModalVCManager?
    var noCellsLeft = false;
    var isFirstRun = true;
    var timeValueChanged = false
    
    var stopWarningController : StopWarningController?
    var settingsViewcontroller : SettingsViewController?

    var secondString, minuteString, hourString : String? ;
    
    @IBOutlet weak var startButton: StartButton!
    @IBOutlet weak var signInButton: SignUpButton!
    @IBOutlet weak var taskButton: TaskButton!    
    @IBOutlet weak var resetButton: UIButton!
    
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet weak var tomato1: TomatoView!
    @IBOutlet weak var tomato2: TomatoView!
    @IBOutlet weak var tomato3: TomatoView!
    @IBOutlet weak var tomato4: TomatoView!
    
    var pomoMinute : Int = 25 {
        didSet{
            timeValueChanged = true;
        }
    };
    var breakMinute : Int = 5 {
        didSet {
            timeValueChanged = true;
        }
    };
    var longBreakMinute : Int = 15 {
        didSet {
            timeValueChanged = true;
        }
    }
    
    var isSignedIn = false {
        didSet {
            if !isSignedIn {
                self.usernameLabel.text = "not signed in"
                if self.settingsViewcontroller?.logoutButton != nil {
                    self.settingsViewcontroller?.logoutButton.setTitle("NOT LOGGED IN", forState: .Normal)
                    if ( self.signInButton != nil) {
                        self.signInButton.showStats = false;
                        self.startButton.showStats = false;
                    }
                    
                }
            } else {
                self.usernameLabel.text  = PFUser.currentUser()?.username
                // self.settingsViewcontroller?.logoutButton.setTitle("LOGOUT", forState: .Normal)
            }
        }
    };

    
    
    //******* VIEW CONTROLLER LIFECYCLE **********
    override func viewDidLoad() {
        super.viewDidLoad()
     
        PomoControllerUtils.cleanUpBackgroundColors(self)
        startButton.addTarget(self, action: PomoEvent.StartButtonClicked, forControlEvents: .TouchUpInside)
       timer = PomoTimer(minute: pomoMinute,
            second: 0 ,
            timerLabel: timerLabel,
            breakMinute: breakMinute,
            breakSecond: 0,
            viewController: self,
            longBreakMinute: longBreakMinute);
        //timer = PomoTimer(minute: 0, second: 10, timerLabel: timerLabel, breakMinute: 0, breakSecond: 5, viewController: self, longBreakMinute: 1);
        
        timer?.reset(); // will reset the clock to the time specified
        
        pomoSession = PomoSession();
        addTomatoesToTimer(timer!);
        timer?.pomoSessionListener = pomoSession;
        stopWarningController = StopWarningController(viewController: self);
        
        modalVCManager = ModalVCManager(viewController: self)
        
        resetButton.enabled = false; // disable until the timer is stopped.
        
        pushButtonsOffScreen();
       
        
        //give a reference to the startbutton so the startbutton can update its text when the signInButton is clicked
        self.signInButton.startButton = self.startButton
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewDidAppear(animated)
       
        resetButtons();
    
        if PFUser.currentUser() != nil {
            // user is logged in , set the username placeholder to his alias
            self.isSignedIn = true;
        } else {
            self.isSignedIn = false
        }
        
        getTimeChangeApprovalFromUser();
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewDidAppear(animated)
        //pushButtonsOffScreen()
        self.startButton.showStats = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //******* BUTTON IBACTIONS **********
    func startButtonClicked(sender: StartButton) {
        //session is about to begin
        if let timer = timer {
            if !timer.isActive {
                
                startButton.enabled = false;
                if(isFirstRun) {
                    self.startButton.resetButton()
                    self.startButton.resetStrokePosition()
                    isFirstRun = false;
                }
                startButton.animationWithHandler("continueAnim", completion: {
                    timer.start();
                    
                    sender.removeTarget(self, action: PomoEvent.StartButtonClicked, forControlEvents: .TouchUpInside)
                    sender.addTarget(self, action: PomoEvent.StopButtonClicked, forControlEvents: .TouchUpInside)
                    
                    self.startButton.enabled = true;
                })
                
                resetButton.enabled = false;
            }
        }
    }
    
    func stopButtonClicked(sender: UIButton) {
        
        if let timer = timer {
            if timer.isActive {
                self.startButton.removeTarget(self, action: PomoEvent.StopButtonClicked, forControlEvents: .TouchUpInside)
                self.startButton.addTarget(self, action: PomoEvent.StartButtonClicked, forControlEvents: .TouchUpInside)
                self.startButton.resetAnimation("resetAnim"); //play the reset animation
                self.timer?.stop();
                self.resetButtons();
                self.resetButton.enabled = true;                 //since we are now finished
                
                
            } else {
                resetButton.enabled = true;
                sender.removeTarget(self, action: PomoEvent.StopButtonClicked, forControlEvents: .TouchUpInside)
    
            }
        }
    }
    
    @IBAction func resetButtonClicked(sender: UIButton) {
        
        if let timer = timer {
            if ( isSignedIn) {
                self.stopWarningController?.showAndActOnWarningMessage()
            } else {
                if (self.timer!.isButtonReset()) {
                    self.animateTimerLabel();
                } else {
                
                    startButton.breakStarted = false;
                    startButton.resetStrokePosition()
                    timer.tomatoAnimator?.resetTomatoes()
                    resetButtons()
                
                    timer.reset();
                }
            }
      }
    }
    
    
    @IBAction func tasksButtonClicked(sender: UIButton) {
        
        modalVCManager!.updateDisplayModalView("TaskViewController")
    }
    
    @IBAction func signInButtonClicked(sender: AnyObject) {
        
        if PFUser.currentUser() == nil {
            self.modalVCManager!.updateDisplayModalView("SignUpViewController")
            return;
        }
        
        self.modalVCManager?.presentViewControllerFullScreen("StatsViewController")
   
    }
    
    //******* TRANSFORMS AND ANIMATIONS **********
     func animateTimerLabel(){
        let largeSize = CGAffineTransformMakeScale(4.8, 4.8);
        let swivelRotation = CGAffineTransformMakeRotation(CGFloat(M_PI/6.0))
        
    // This is the animation when the button is clicked. It will make a small pivot rotation with a positive radian rotation of pi/6
        UIView.animateWithDuration(0.3, delay: 0, usingSpringWithDamping: 2.3, initialSpringVelocity: 0.3, options: [], animations: {
            self.timerLabel.transform = largeSize
            self.timerLabel.transform = swivelRotation
            
            }, completion: { done in
                UIView.animateWithDuration(0.2, delay: 0, usingSpringWithDamping: 2.3, initialSpringVelocity: 0.3, options: [], animations: {
                    self.timerLabel.transform = CGAffineTransformIdentity
                    }, completion: nil)
        })
    }
 
    
    func resetButtons() {

        // reset the buttons position
        UIView.animateWithDuration(0.5, delay: 0.2, usingSpringWithDamping: 0.7 , initialSpringVelocity: 1.4, options: UIViewAnimationOptions.CurveEaseInOut , animations: {
            self.startButton.transform = CGAffineTransformIdentity;
            self.resetButton.transform = CGAffineTransformIdentity;
            self.taskButton.transform = CGAffineTransformIdentity;
            
            }, completion: nil );
    }

    
    func pushButtonsOffScreen() {
        
        let viewBounds = self.view.bounds
      
        let rightOffset = CGAffineTransformMakeTranslation(viewBounds.width/2, 0)
        let leftOffset = CGAffineTransformMakeTranslation(-viewBounds.width, 0)
        let topOffset = CGAffineTransformMakeTranslation(0, -viewBounds.height)
    
        
        startButton.transform = topOffset;
        resetButton.transform = leftOffset
        taskButton.transform = rightOffset;
        
        //resetButton.transform = resetRotation
        //taskButton.transform = taskRotation
        
    }
    
    //******* DELEGATE TIMER SET UP **********
    func setTime(time: Int) -> Int {
       
       
       
        pomoMinute = pomoMinute == 0 ? 25 : time
        breakMinute = breakMinute == 0 ? 5 : breakMinute;
        longBreakMinute = longBreakMinute == 0 ? 15 : longBreakMinute

        canChangeValue = false;
        
        return pomoMinute
    }
    
    
    
    var canChangeValue = false;
    func getTimeChangeApprovalFromUser() {
        if timeValueChanged {
            print("Value Changed", terminator: "");
            if (PFUser.currentUser() != nil) {
                stopWarningController?.showAndActOnWarningMessage()
               timeValueChanged = false;
            } else {
                resetButtons()
                stopTimer()
                rebuildTheTimer()

            }
        }
    }
    
    func setBreakTime(time: Int) -> Int {
        
        pomoMinute = pomoMinute == 0 ? 25 : pomoMinute; // if we haven't set the timer, set it to 25 , or set it to it's last known value.
        breakMinute = breakMinute == 0 ? 5 : time; // if we havent set the break. set it to 5 minutes
        longBreakMinute = longBreakMinute == 0 ? 15 : longBreakMinute
     
        return breakMinute;
    }
    
    
    
    func setLongBreakTime(time: Int) -> Int {
        
        pomoMinute = pomoMinute == 0 ? 25 : pomoMinute
        breakMinute = breakMinute == 0 ? 5 : breakMinute   // if we haven't set the break, set it to 5 , or set it to it's last known value.
        longBreakMinute = longBreakMinute == 0 ? 15 : time
    
        return longBreakMinute;
    }
    
    // set the time of the pomodoro minute here
    func stopTimer() {
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        appDelegate.timer?.invalidate();
        if(timer!.isActive) {
            self.stopButtonClicked(self.startButton) //timer was active before, make sure it is stopped.
        }
    }
    
    func rebuildTheTimer() {
        self.startButton.resetAnim();
        timer = PomoTimer(minute: pomoMinute, second: 0, timerLabel: timerLabel, breakMinute: breakMinute,
                                         breakSecond: 0, viewController: self, longBreakMinute :longBreakMinute);
        timer?.pomoSessionListener = pomoSession;
        
        PomoControllerUtils.timerResetHelper(timer!, controller: self)
    }
    
    func addTomatoesToTimer(timer : PomoTimer) {
        timer.addTomatoes(tomato1, tomato2: tomato2, tomato3: tomato3, tomato4: tomato4)
    }
 
}

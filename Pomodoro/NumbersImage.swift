//
//  NumbersImage.swift
//  Task Timer
//
//  Created by Andrew Lincoln on 7/18/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class NumbersImage: UIView {

    
    override func drawRect(rect: CGRect) {
        PomoUtils.drawNumbers(frame: rect)
    }
}

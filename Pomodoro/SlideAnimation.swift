//
//  SlideAnimation.swift
//  Task Timer
//
//  Created by Andrew Lincoln on 7/17/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class SlideAnimation: NSObject, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate {
    var presenting : Bool = false;
    
    
    
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
      //  let fromViewCont = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)
     //   let toViewCont = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
        let fromView = transitionContext.viewForKey(UITransitionContextFromViewKey)
        let toView = transitionContext.viewForKey(UITransitionContextToViewKey)
        
        toView?.transform = CGAffineTransformMakeRotation(CGFloat(M_PI/6.0))
        
        let containerView = transitionContext.containerView();
        
        let offScreenRight = CGAffineTransformMakeTranslation(containerView!.frame.width, 0)
        let offScreenLeft = CGAffineTransformMakeTranslation(-containerView!.frame.width, 0)
        toView?.transform = self.presenting ? offScreenRight : offScreenLeft
    
         containerView!.addSubview(fromView!)
        containerView!.addSubview(toView!)
       
        
        let duration = self.transitionDuration(transitionContext)
        
        UIView.animateWithDuration(duration, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .CurveEaseIn, animations: {
            
           
            toView?.transform = CGAffineTransformIdentity
            
            }, completion: {done in
                
                transitionContext.completeTransition(true)
            
        })
        
        
        
        
    }
    
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.5
    }
    
    
    
    
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.presenting = false;
        return self;
    }
    
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        self.presenting = true;
        return self;
    }
}

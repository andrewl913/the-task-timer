//
//  TomatoAnimator.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 6/16/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class TomatoAnimator: NSObject {
  
  var filledTomatoImage =  UIImage(named: "filledTomato")
  var emptyTomatoImage = UIImage(named: "emptyTomato")
  var tomatoes = Array<TomatoView>()
  
  
  // ******** INITIALIZERS ********
  
  init(tomato1 : TomatoView, tomato2 : TomatoView , tomato3 : TomatoView, tomato4 : TomatoView) {
    
    tomatoes.append(tomato1);
    tomatoes.append(tomato2);
    tomatoes.append(tomato3);
    tomatoes.append(tomato4);
  }
  
  func resetTomatoes() {
    for tomato in tomatoes {
      tomato.backgroundColor = UIColor.clearColor();
      
      if tomato.alpha == 1 {
        UIView.animateWithDuration(0.3, animations: {
          tomato.alpha = 0;
          
          }, completion: { done in
            tomato.pomoIncrement = false;
        })
      }
      
      
    }
  }
  
  func checkAndFillTomatoes(pomoCounter : Int) {
    switch pomoCounter {
    case 1:
      animateTomato(tomatoes[0])
      scaleTomato(tomatoes[0], duration: 0.2)
      break;
    case 2:
      animateTomato(tomatoes[1])
      scaleTomato(tomatoes[1], duration: 0.2)
      break;
    case 3:
      animateTomato(tomatoes[2])
      scaleTomato(tomatoes[2], duration: 0.2)
      break;
    case 4:
      animateTomato(tomatoes[3])
      scaleTomato(tomatoes[3], duration: 0.2)
      break;
    default:
        break;
    }
  }
  
  func fullCycleAnimation(time : NSTimeInterval) {
    resetTomatoImages(false);
    //animate tomato1
    scaleTomato(self.tomatoes[0], duration: time * 1/8 );
    
    //animate tomato2
    scaleTomato(self.tomatoes[1], duration: time * 2/8);
    
    //animate tomato3
    scaleTomato(self.tomatoes[2], duration: time * 3/8)
    
    //animate tomato4
    scaleTomato(self.tomatoes[3], duration: time * 4/8)
    
    //animate tomato1
    descaleTomato(self.tomatoes[0], duration: time * 5/8);
    
    //animate tomato2
    descaleTomato(self.tomatoes[1], duration: time * 6/8);
    
    //animate tomato3
    descaleTomato(self.tomatoes[2], duration: time * 7/8);
    
    //animate tomato4
    descaleTomato(self.tomatoes[3], duration: time * 8/8);
    
  }
  
  private func scaleTomato(tomato: TomatoView, duration : NSTimeInterval) {
    let largeScale = CGAffineTransformMakeScale(1.2, 1.2);
    UIView.animateWithDuration(duration, animations: {
      tomato.transform = largeScale;
    });
  }
  
  private func descaleTomato(tomato: TomatoView, duration : NSTimeInterval) {
    let originalSize = CGAffineTransformIdentity;
    UIView.animateWithDuration(duration, animations: {
      tomato.transform = originalSize;
    })
  }
  
  private func animateTomato(tomato :TomatoView) {
    
    if tomato.pomoIncrement == true {
      return;
    }
    tomato.pomoIncrement = true;
    
    tomato.alpha = 0;
    UIView.animateWithDuration(0.3, animations: {
      tomato.alpha = 1;
      
    })
  }
  
  func resetTomatoImages(resetColor : Bool){
    if resetColor {
      
      animateTomato(tomatoes[0])
      animateTomato(tomatoes[1])
      animateTomato(tomatoes[2])
      animateTomato(tomatoes[3])
    }
    
    for tomato in tomatoes {
      UIView.animateWithDuration(0.5, animations: {
        tomato.transform = CGAffineTransformIdentity  // transform to original position
      })
    }
  }
  
}

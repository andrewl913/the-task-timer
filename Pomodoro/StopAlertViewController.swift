//
//  StopAlertController.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 6/22/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class StopAlertViewController: UIViewController {
  
  
  @IBOutlet weak var confirmLabel: UILabel!
  @IBOutlet weak var warningLabel: UILabel!

  init() {
    super.init(nibName: "", bundle: nil)
  
  }

  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
      //fatalError("init(coder:) has not been implemented")
    print("Not in use")
    
  }
}

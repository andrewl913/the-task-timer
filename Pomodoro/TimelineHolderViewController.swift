//
//  TimelineHolderViewController.swift
//  Task Timer
//
//  Created by Andrew Lincoln on 7/16/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class TimelineHolderViewController: UIViewController {

    var timelineViewManager : TimelineViewManager?;
    var timelineTableViewController : TimelineTableViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        timelineTableViewController = findTableViewController();
        timelineTableViewController?.timeLineViewManager = self.timelineViewManager
        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var dismissButton: UIButton!
    @IBAction func dismissTimeline(sender: UIButton) {
        
        self.dismissViewControllerAnimated(true, completion: {})
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func findTableViewController() -> TimelineTableViewController? {
        for  viewController in self.childViewControllers {
            if viewController is TimelineTableViewController {
                return viewController as? TimelineTableViewController
            }
        }
        return nil
    }

}

//
//  DetailViewManager.swift
//  Task Timer
//
//  Created by Andrew Lincoln on 7/11/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit
import Parse

class TimelineViewManager: NSObject, UITableViewDelegate {
    
    var timelineTableViewController : TimelineTableViewController
    var statsTableViewController : StatsTableViewController
    var timelineHolderViewController : TimelineHolderViewController
    
    var detailCells : [DetailViewCell] = []
    var days : [String] = []
    var dates : [NSDate] = []
    var statValues :[Int] = []
    var statRatios : [Double] = []
    var largestNumber : Double = 0;     //sum of the values from the pass 5 days.
    var isPopulating = false;         //boolean value that tells the stat button that a query is taking place
    var graphRange  : CGFloat = 225;      //lower values mean more values are shown.
    let numberOfDaysToPopulate = 6;
    let daysInAWeek = 7
    var queryKey : String = "";
    
    var calendar = NSCalendar.currentCalendar();
    var animationCount = 0;
    
    
    init(timelineTableViewController  : TimelineTableViewController, statsTableViewController : StatsTableViewController, timelineHolderViewController : TimelineHolderViewController) {
        self.timelineHolderViewController = timelineHolderViewController
        self.timelineTableViewController = timelineTableViewController
        self.statsTableViewController = statsTableViewController
        super.init()
        self.detailCells = timelineTableViewController.detailCells
        populateTheDays();
       // populateDataForDetailCells(self.detailCells)
        
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : DetailViewCell = tableView.dequeueReusableCellWithIdentifier("DetailViewCell") as! DetailViewCell
    
        let statValue = statValues[indexPath.row];
        cell.progressBar.position = -200
        
        populateDateLabelsForCell(cell, index: indexPath.row)
    
        cell.progressIntLabel.text =  queryKey == "overallSessionTime" ? self.statsTableViewController.secondsToTime(statValue) : "\(statValue)"
    
     
        print("ratio \(statValue)/\(largestNumber/5))")
        
    
    
        let average = largestNumber/5.0
        cell.progressBar.position = CGFloat((Double(statValue)/4.0 / average))
        
        cell.selectionStyle = .None
        animationCount++;
        
        
        return cell;
    }
    
    
    

 
    func populateTheDays () {
        
        let calandarUnit: NSCalendarUnit = [NSCalendarUnit.Day, .Month, .Year, .Hour, .Minute, .Second]
        
        let dateComponents = NSCalendar.currentCalendar().components(calandarUnit, fromDate: NSDate())
        dateComponents.timeZone = NSTimeZone(name: "GMT")
        
        let formatter = NSDateFormatter();
        formatter.timeZone = NSTimeZone(name: "GMT")
        formatter.dateFormat = "MMM dd yyyy, hh:mm"
        
        for var i = 0 ; i < numberOfDaysToPopulate; i++ {
            // append the last 14 days to the NSDate array
            let date = calendar.dateWithEra(1, year: dateComponents.year, month: dateComponents.month , day: dateComponents.day - i, hour: dateComponents.hour, minute: dateComponents.second, second: dateComponents.second, nanosecond: 0 )!
            let dateString = formatter.stringFromDate(date)
            days.insert(dateString, atIndex: i)
            dates.insert(date, atIndex: i);
            
            
        }
    }
    
    
    func populateTheTableView(key : String) {
        isPopulating = true;
        queryKey = key;
        let backgroudQueue = NSOperationQueue();
        backgroudQueue.addOperationWithBlock({
      // var index = 0;
            
            for day in self.dates {
                print(day)
                self.getCountForKey(key, day: day)
                // on the main thread, update the UI
                
            }
           
            NSOperationQueue.mainQueue().addOperationWithBlock({
                
              
                self.statsTableViewController.presentViewController(self.timelineHolderViewController, animated: true, completion: {})
                
                self.timelineTableViewController.tableView.reloadData()
                self.statsTableViewController.resetCellButtonState()
                self.isPopulating = false;
                
            })
            
        })
        
        
    }
    
    // pass in the day and add up the stats for that day with a given key
    func getCountForKey(key: String, day :NSDate) {
      //  let sessionQuery = PFQuery(className: "PomoSession")
        let otherDay = NSDateComponents();
        
        otherDay.day  = -1
        
        let yesterday = NSCalendar.currentCalendar().dateByAddingComponents(otherDay, toDate: day, options: [] )
        let params = NSMutableDictionary() // return the count for a given day
        
        params["key"] = key;
        params["day"] = day;
        params["yesterday"] = yesterday;
        
        let sum = PFCloud.callFunction("timeline", withParameters: params as [NSObject : AnyObject]) as? Int;
        if let sum = sum {
            largestNumber+=Double(sum);
            self.statValues.append(sum)
        }
  
        // we are at the last value in stat values and will have no index to compare.. just add a value to prevent array out of bounds exception
        if(self.statValues.count == numberOfDaysToPopulate) {
            self.statRatios.append(0);
        }
        
        
        print("Sum \(sum)");
        
    }
    func populateDataForDetailCells (detailCells: [DetailViewCell]) {
        for var i = 0 ; i < numberOfDaysToPopulate - 1 ; i++ {
            let statValue = statValues[i]
            populateDateLabelsForCell(detailCells[i], index: i);
            detailCells[i].progressIntLabel.text =  queryKey == "overallSessionTime" ? self.statsTableViewController.secondsToTime(statValue) : "\(statValue)"
            detailCells[i].selectionStyle = .None
            let average = largestNumber/5.0
            detailCells[i].progressBar.position = CGFloat((Double(statValue)/4.0 / average))
            detailCells[i].progressBar.startProgressAnimation()

        }
        
        
    }
    
    
    func populateDateLabelsForCell(cell : DetailViewCell, index : Int) {
        let formatter = NSDateFormatter();
        formatter.timeZone = NSTimeZone.localTimeZone();
        formatter.dateFormat = "MMM dd"
        
        let dateString  = formatter.stringFromDate(dates[index])
        
        switch index {
        case 0:
            cell.dateLabel.text = "Today"
            break;
        case 1:
            cell.dateLabel.text = "Yesterday"
            break;
        default:
            cell.dateLabel.text = dateString
            break;
        }
        
    }
    
    
    func updateTitleForKey(key : String) {
        let tableTitle = self.timelineTableViewController.tableTitle
        switch key {
        case "overallSessionTime":
            tableTitle.text = "OVERALL SESSION TIME"
            break;
        case "stopCount":
            tableTitle.text = "STOP COUNT"
            break;
        case "tasksFinishedCount":
            tableTitle.text = "TASKS COMPLETE"
            break;
        case "numberOfTomatoes":
            tableTitle.text = "NUMBER OF TOMATOES"
            break;
        default:
            break;
        }
        
    }
    
    func resetGraph() {
        detailCells.removeAll(keepCapacity: true)
    }
    
   
    func largestNumber(num1: Int, num2 : Int) -> Int {
       return  num1 >= num2 ? num1 : num2
    }
    
    func clearData() {
        statValues  = []
         statRatios  = []
        largestNumber = 0;
        animationCount = 0;
    }
    
}

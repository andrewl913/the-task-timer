 //
 //  PomoTimer.swift
 //  Pomodoro
 //  v0.5
 //  Created by Andrew Lincoln on 5/17/15.
 //  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
 //
 
 import UIKit
 import AVFoundation
 import Parse
 
 protocol TimerFireDelegate {
    func timerToFire(timer : NSTimer, pomoTimer : PomoTimer);
 }
 
 protocol PomoEventListenable {
    func stop();
    func start()
    func end();
    func breakStarted();
    func longBreakStarted();
    func timeIncremented();
    func taskAdded(outOf : Int);
    func taskFinished(outOf : Int)
 }
 
 class PomoTimer: NSObject {
    
    var timerLabel : UILabel
    var pomoViewController : UIViewController
    var num = [0,4,5]
    
    var tomatoAnimator : TomatoAnimator?
    var pomoSessionListener : PomoEventListenable?
    
    //***** GESTURE RECOGNIZER 
    
    var swipeGesture : UISwipeGestureRecognizer?
    var isFirstTimeResetting = true;
    
    
    //******** TRACKERS ***************
    var deltaTracker = 0;
    
    var pomoCounter = 0,
    pomoHalfCounter = 0,
    queueTracker = 0,
    hour = 0,
    absoluteTime = 0,
    absoluteTimeStatic = 0,
    userSetMinute,
    userSetSecond,
    userBreakMinute,
    userLongBreakMinute,
    userBreakSecond : Int;
    
    let longBreakFlag = 9;  // the amount of iterations the pomoHalfCounter makes to trigger the long break
    
    var tomatoArray : [UIImageView]?
    let minimumTimeBeforeSessionInitializes : Int = 5;
    
    
    //SoundEffect
    var bellFile : NSURL = NSBundle.mainBundle().URLForResource("Desk-bell-sound", withExtension: "wav")!
    var avPlayer : AVAudioPlayer?
    
    // API checks
    var isFinished = false,
    isOnBreak = false,
    isActive = false,
    wasActive = false,
    didIncludeZero = false, //to include the zero second as a component of time
    isCountingUp = false,
    isOnLongBreak = false,
    canPause = false,
    canPlaySound = true,
    labelWasSwipedLeft = false;
    
    // ***** PROPERTY OBSERVERS *****
    
    var minute : Int {
        didSet {
            if minute > 59 {
                self.hour++;
                self.minute = 0;
            } else if minute < 0 {
                self.hour--;
                self.minute = 59;
            }
        }
    }
    
    var second : Int {
        didSet {
            if self.isActive {
                pomoSessionListener?.timeIncremented();      //let the session know that the timer decremented to log out the total time of the session

            }
            if second > 59 {
                self.minute++;
                self.second = 0;
            } else if second < 0 {
                self.minute--;
                self.second = 59;
            }
        }
    }
    
    private var timer : NSTimer?
    var timerDelegate : TimerFireDelegate?
    
    
    //***** INITIALIZERS *****
    override init() {
        self.minute = 0
        self.second = 0
        self.timerLabel = UILabel();
        self.userSetMinute = 59;
        self.userSetSecond = 59;
        self.userBreakMinute = 1;
        self.userBreakSecond = 59;
        self.userLongBreakMinute = 15;
        self.pomoViewController = UIViewController();
        
        do {
            self.avPlayer = try AVAudioPlayer(contentsOfURL: bellFile)
        } catch let error1 as NSError {
            print(error1.userInfo);
            self.avPlayer = nil
        }
        self.avPlayer?.prepareToPlay()
        
    }
    // send in the timer label to manipulate the output
    init( minute: Int, second: Int, timerLabel : UILabel, breakMinute : Int, breakSecond: Int, viewController : UIViewController, longBreakMinute: Int) {
        
        self.timerLabel = timerLabel;
        self.pomoViewController = viewController;
        self.userSetMinute = minute;          // set the minute
        self.minute = userSetMinute;         // reference the user set minute everytime we reset the timer.
        self.userSetSecond = second;
        self.second = userSetSecond;
        self.userBreakSecond = breakSecond
        self.userBreakMinute = breakMinute
        self.userLongBreakMinute = longBreakMinute
        self.absoluteTime = minute * 60;
        self.absoluteTimeStatic = absoluteTime;
        super.init();
        self.absoluteTimeStatic = setAbsoluteTime();
        
        
           // error pointer needed in constructor
        do {
            self.avPlayer = try AVAudioPlayer(contentsOfURL: bellFile)
        } catch let error1 as NSError {
            print(error1.userInfo)
            self.avPlayer = nil
        }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        } catch _ {
        }
        do {
            try AVAudioSession.sharedInstance().setActive(true)
        } catch _ {
        }
        UIApplication.sharedApplication().beginReceivingRemoteControlEvents()
        self.avPlayer?.prepareToPlay();                  // prepare to play will load the audio before we need to play the sound.
        
        self.timerDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.timerLabel.userInteractionEnabled = true;
 
        
    }
    
    
    func addTomatoes(tomato1 : TomatoView, tomato2 : TomatoView, tomato3 : TomatoView, tomato4 : TomatoView) {
        tomatoAnimator = TomatoAnimator(tomato1: tomato1, tomato2: tomato2, tomato3: tomato3, tomato4: tomato4)
        
        tomatoAnimator?.resetTomatoes(); // reset the tomatoes to a clear color ( invisible )
    }
    
    //*************** TIMER FUNCTIONS ********************
    func fireTimer() {
        
        if let pomoViewController = pomoViewController as? MainViewController {
            //get the absolute time by multipying the userSetMinute by 60
            
            
            pomoViewController.startButton.startAnimation(timer: self, second: absoluteTime, deltaTime: CGFloat(deltaTracker) , breakSecond: absoluteTime, outOf: absoluteTimeStatic) // animate the startButton
            
            if deltaTracker % 60 == 0 {
                deltaTracker = 0;
                
                if  absoluteTime <= absoluteTimeStatic - minimumTimeBeforeSessionInitializes {
                    pomoSessionListener?.start()
                }
                
                // print("Current Angle Position : \(currentPosition) Current Absolute Time: \(self.absoluteTime) Absolute Time \(self.absoluteTimeStatic)\n");
                decrementOperation();
            }
            
            deltaTracker++;
        }
    }
    
    //start the timer
    func start() {
      
        self.isActive = true;
        
        timer = NSTimer.scheduledTimerWithTimeInterval(1/60, target: self,
            selector: Selector("fireTimer"),
            userInfo: nil,
            repeats: true) // decrement
        
        //place the timer on a background thread to prevent any events on UIResponder chain from locking up the timer.
        NSRunLoop.mainRunLoop().addTimer(timer!, forMode: NSRunLoopCommonModes)
        
        
        if let timer = self.timer {
            timerDelegate?.timerToFire(timer, pomoTimer: self);       //set the degelate
            timer.fire()
          
            } else {
            print("Error firing the timer", terminator: "")
        }
    }
    
    //stop the timer
    func stop() {
        pomoSessionListener?.stop()
        
        self.isActive = false;
        self.wasActive = true;
        if let timer = self.timer {
            // remove the timer from the background thread
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.timer?.invalidate();
            timer.invalidate()
            didIncludeZero = false;
        }
    }
    
    //reset the timer
    func reset() {
        
        if isFirstTimeResetting && absoluteTime > 60 {
            //if it is the first time resetting.. we don't want the session count to inrease until another run.
            // a session is only valid if it is longer than sixty minutes
            if(self.wasActive) {
                pomoSessionListener?.end();
            }
            isFirstTimeResetting = false;
        }
       
        self.hour = 0;
        self.second = userSetSecond;
        self.minute = userSetMinute;
        self.isOnBreak = false; // we are not on break;
        self.pomoCounter = 0;        // clear all pomos
        self.pomoHalfCounter = 0;    // clear half pomo markers
        self.absoluteTime = userSetMinute * 60; // reset to the stored value
        self.absoluteTimeStatic = absoluteTime
        updateTimerDisplay();
        
    }
    
    // set the absolute time in regards to break / pomo / or long break.
    func setAbsoluteTime() -> Int {
        
        let lengthOfHour = 60;
        if(isOnLongBreak){
            self.absoluteTime = userLongBreakMinute * lengthOfHour;
        } else if (isOnBreak) {
            self.absoluteTime = userBreakMinute * lengthOfHour;
        } else {
            self.absoluteTime = minute * lengthOfHour;
        }
        return self.absoluteTime
    }
    
    //************ BACKGROUND COLOR ANIMATIONS **********************************
    
    var primeColor = UIColor(red: 101/255, green: 120/255, blue: 135/255, alpha: 1); // greyish
    var breakColor = UIColor.whiteColor(); // off-white
    
   
    
    //************ BUTTON METHODS ******************************
    
  
    func isButtonReset() -> Bool {
        return absoluteTime == absoluteTimeStatic; //check if we are at the beginning of our countdown
    }
    
    //************ DECREMENT OPERATION METHOD *******************
    func decrementOperation() {
        second--;
        absoluteTime--; // subtract the absolute time as well as the displays second
        self.didIncludeZero = true; // we included zero marker, continue with default operation
       
        self.updateTimerDisplay();
    
        
        self.breakCheck();        // check if it is the break
        self.longBreakCheckAndAnimations();        // check if we are on the long break
        
    }
    
    // ************ DECREMENT OPERATION DEPENDENCIES ****************
    
    private func longBreakCheckAndAnimations() {
        // if the pomoHalfCounter is 9 that means we are starting the long break.
        // animate the tomatoes to signal to the user that we are on our long break
        if pomoHalfCounter == longBreakFlag {
            tomatoAnimator?.fullCycleAnimation(3);
            pomoCounter = 0;
            pomoHalfCounter = 0;
          
            pomoSessionListener?.longBreakStarted();
            isOnLongBreak = true;
            
        }
        if(isOnLongBreak) {
            //tell the listener that the long break started
           
            if absoluteTime % 10 == 0 {
                tomatoAnimator?.fullCycleAnimation(3)
                
            }
        }
        if(pomoHalfCounter == 1 && isOnLongBreak) {
            tomatoAnimator?.resetTomatoes();
            isOnLongBreak = false;
            isOnBreak = false;
        }
        
    }
    
    private func breakCheck() {
        if hour == 0 && minute == 0 && second == 0 {
            pomoHalfCounter++;// we have reached half a pomodoro. increment and start a break//
            if canPlaySound {
                avPlayer?.play()
            } else {
                avPlayer?.stop()
            }
        
            self.minute = pomoHalfCounter % 2 == 0 ? self.userSetMinute : pomoHalfCounter == longBreakFlag ? self.userLongBreakMinute : self.userBreakMinute
            self.second = pomoHalfCounter % 2 == 0 ? self.userSetSecond : pomoHalfCounter == longBreakFlag ? 0 : self.userBreakSecond
            self.absoluteTime = self.minute * 60
            
            self.absoluteTimeStatic = absoluteTime
            
            
            if pomoHalfCounter % 2 == 0 {
                isOnBreak = false;
                pomoCounter++;
                print("Pomos: \(pomoCounter)\n", terminator: "")
                
            } else {
                // tell the listener that the breakstarted
                 pomoSessionListener?.breakStarted();
                 isOnBreak = true;
                
            }

            
            isFinished = true;
        }
        
        tomatoAnimator!.checkAndFillTomatoes(pomoCounter);
    }
    
  
    //*********** Tomato Animations *******************
    
    var eleCounter = 0; //dependency for animateAllTomatoes
    
    func animateAllTomatoes(tomato : [UIImageView], element : Int, directionSwitch: Bool) {
        
        let vertOffset = directionSwitch ? CGAffineTransformMakeTranslation(0, -7) : CGAffineTransformMakeTranslation(0, 7);
    
        UIView.animateWithDuration(0.5, animations: {
            if(element < tomato.count) {
                tomato[element].transform = vertOffset
            }
            
            }, completion: { done in
                if element < tomato.count {
                    UIView.animateWithDuration(0.5, animations: {}, completion: { done in
                        
                        self.animateAllTomatoes(tomato, element: element + self.eleCounter, directionSwitch : self.eleCounter % 2 == 0) // switch directions
                        self.eleCounter++;
                        
                    })
                } else {
                    self.eleCounter = 0;
                    
            }
        })
    }
    
    func updateTimerDisplay() {
        if(self.minute < 10 && self.second < 10) {
            self.timerLabel.text =  "\(hour):0\(minute):0\(second)"; // second needs a zero prepended
        } else if self.minute < 10  {
            self.timerLabel.text =  "\(hour):0\(minute):\(second)"; // minute needs a zero prepended
        } else if self.second < 10 {
            self.timerLabel.text =  "\(hour):\(minute):0\(second)"; // both need zero prepended
        } else {
            self.timerLabel.text =  "\(hour):\(minute):\(second)"; // update the view every second
        }
    }
    
    
    func secondsToTime(seconds: Int) -> String {
        let hour = seconds/3600
        let minute = (seconds % 3600)/60
        let second = (seconds % 3600) % 60
        
        if(minute < 10 && second < 10) {
            return "\(hour):0\(minute):0\(second)"; // second needs a zero prepended
        } else if minute < 10  {
            return  "\(hour):0\(minute):\(second)"; // minute needs a zero prepended
        } else if second < 10 {
            return  "\(hour):\(minute):0\(second)"; // both need zero prepended
        } else {
            return "\(hour):\(minute):\(second)"; // update the view every second
        }
        
    }
    
 }

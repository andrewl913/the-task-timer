//
//  TaskStorageManager.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 7/7/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit
import Foundation

protocol NSCodable {
    typealias T
    func save()
    func clear()
  static func loadData() -> T?
    
}

class PomoTaskStorageManager: NSObject , NSCoding , NSCodable {
    
    var taskList : [PomoTask] = []
    typealias T = PomoTaskStorageManager
    
    
    override init() {
        self.taskList = [];
    }
    required init?(coder aDecoder: NSCoder) {
        taskList = aDecoder.decodeObjectForKey("taskList") as! [PomoTask]
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.taskList, forKey: "taskList")
    }
    
    func save() {
        let archiver = NSKeyedArchiver.archivedDataWithRootObject(self)
        NSUserDefaults.standardUserDefaults().setObject(archiver, forKey: "pomoTaskStorage")
    }
    
    func clear() {
        NSUserDefaults.standardUserDefaults().removeObjectForKey("pomoTaskStorage")
    }
    
   class func loadSavedManager() -> PomoTaskStorageManager? {
    
        return loadData()
    }
    
    
    static func loadData() -> PomoTaskStorageManager? {
        if let data = NSUserDefaults.standardUserDefaults().objectForKey("pomoTaskStorage") as? NSData {
            return NSKeyedUnarchiver.unarchiveObjectWithData(data) as?  PomoTaskStorageManager
        }
        
        return nil;

    }

}



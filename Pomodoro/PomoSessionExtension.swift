//
//  PomoSessionExtension.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 7/8/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit
import Parse

extension PomoSession {
    
    func discardTasks() {
        
        
        if let taskStorageManager = PomoTaskStorageManager.loadSavedManager() {
            
            taskStorageManager.clear();
        }
    }
    
    
    func transferStatsToLastSession() {
        if let pomoSession = PomoSession.loadData() {
            //pomoSession.save();
            lastPomoSession = pomoSession;
           // lastPomoSession?.save()
        }
        
    }
    
    func discardOldSessions(session : PomoSession) {
        if session.lastPomoSession!.Id > 14 {
            session.lastPomoSession = nil;
            return
        }
    
        discardOldSessions(session)
    }
   
 
    
   
    
 
 
    
    
     func saveData(object : PFObject) {
    
        NSOperationQueue().addOperationWithBlock {
            object.saveEventually({ success, error in
                if error == nil {
                    print("Saving")
                }
            
            })
        }
    }
    
    private func findObjects(query : PFQuery) -> [PFObject] {
        var pfObjects : [PFObject] = []
        
        query.findObjectsInBackgroundWithBlock({ results, error in
            if error == nil {
                
                if let objects = results as? [PFObject] {
                    pfObjects = objects
                }
            }
        })
        return pfObjects
    }
    
    
    //****** PUBLIC INTERFACE ********
    
    func addTasks(task : [String])  {
        
        // pomoSessionObject = PFObject(className: "PomoSession")
       // var sessionQuery = PFQuery(className: "PomoSession")
        if PFUser.currentUser() != nil {
            
            print("Found Tasks", terminator: "")
            if let pomoSession = sessionObject {
                pomoSession.addObjectsFromArray(task, forKey: "tasks")
                //pomoSession.save();
            } else {
                print("Pomo Session is invalid \n", terminator: "")
            }
        }
        
        
    }
    
    func retrieveLifeActivityDataFromParse(lifeActivityData : String ) -> Int {
        if let user = PFUser.currentUser() {
            if (user.objectForKey(lifeActivityData) != nil) {
                let data : Int = PFUser.currentUser()?.objectForKey(lifeActivityData) as! Int
                print("LIFE DATA: \(data)\n", terminator: "");
                return data
            }
            
        }
        return 0;
    }
}

//
//  ProgressBarView.swift
//  Task Timer
//
//  Created by Andrew Lincoln on 7/14/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class ProgressBarView: UIView {
   private var progressValue : CGFloat =  -200  {
        
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    var maximumSize : CGFloat = -01;
    
    var deltaTime : CGFloat = -200;
    
    var progressTimer : NSTimer?
    var position : CGFloat = 0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    
    
    
    override func drawRect(rect: CGRect) {
        PomoUtils.drawProgressBar(frame: rect, progressValue: progressValue)
    }
    
    
    func drawProgressView() {
        //start progress at -200 and go up to its percent relative to the maximum size of the progressValue which is 0
        //translate too the position given the fraction is over 0
        
        let targetPosition = CGFloat(1 - position)  * -200
        //println("target position: \(position)");
        if (progressValue  >= targetPosition) {
            if let timer = progressTimer {
                timer.invalidate();
              // bounceBarAnimation()
                
            }
           
            deltaTime = -200;
            return;
        }
        print("position: \(targetPosition)")
        if (progressValue >= 0) {
            if let timer = progressTimer {
                timer.invalidate();
              // bounceBarAnimation()
                
            }
            
            
            deltaTime = -200;
            return;
        }
        
       
        progressValue = deltaTime;
        deltaTime += 0.7
        
        
    }
    
    
    func startProgressAnimation() {
        progressTimer = NSTimer.scheduledTimerWithTimeInterval(1/60, target: self, selector: "drawProgressView", userInfo: nil, repeats: true)
        NSRunLoop.mainRunLoop().addTimer(progressTimer!, forMode: NSRunLoopCommonModes)
        progressTimer!.fire();
    }
    func reset() {
        progressValue = -200
    }
    
}

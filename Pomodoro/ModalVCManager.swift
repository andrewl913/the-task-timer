//
//  ModalVCManager.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 6/27/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class ModalVCManager: NSObject {
    
    // boolean used to set remove the task if false.
    var taskIsHidden : Bool {
        didSet {
            if(!taskIsHidden) {
                removeModalView();
            }
        }
    }
    
    var parentViewController : MainViewController?
    var parentTaskViewController : TaskViewController?
    var doneButton : DoneButton?
    
    init(viewController : MainViewController) {
        self.taskIsHidden = true;
        self.parentViewController = viewController
    }
    
    private func findModalView() -> [UIViewController]? {
                
        return parentViewController!.childViewControllers
        
    }
    
    func getDoneButton() -> DoneButton? {
        let modalViewControllers = findModalView();
        for viewController in modalViewControllers! {
            if viewController is DoneViewController {
                print("FOUND DONE BUTTON")
                return (viewController as? DoneViewController)?.getDoneButton()
            }
        }
        return nil;
    }
    
    private func removeModalView() {
        let modalViewControllers = findModalView();
        for viewController in modalViewControllers! {
            animateModalWindow(viewController, isRemoval: true)
        }
        removeTint();
    }
    
    func removeDoneButton() {
        let modalViewControllers = findModalView();
        for viewController in modalViewControllers! {
            if(viewController is DoneViewController) {
                animateModalWindow(viewController, isRemoval: true)
            }
        }

    }
    
    func animateModalWindow(viewController : UIViewController, isRemoval : Bool) {
        let scale = CGAffineTransformMakeScale(0.01, 0.01);
        let radian = M_PI/6
        let angle = CGAffineTransformMakeRotation(CGFloat(radian))
        let offScreen = CGAffineTransformMakeTranslation(viewController.view.frame.width * 1.3, 0)
        let viewControllerView = viewController.view;
        //if we are removing the view from the superview
        if isRemoval {
             viewControllerView.transform = CGAffineTransformIdentity
            if viewController is DoneViewController {
                PomoAnimations.animationViewPositionWithCompHandler(viewControllerView, toPosition: scale, duration: 0.7, completion: {
                
                    viewControllerView.removeFromSuperview();
                    viewController.removeFromParentViewController();
                    viewController.dismissViewControllerAnimated(false, completion: nil)

                })
            } else {
           
            PomoAnimations.animationViewAngle(viewControllerView, angle: angle, duration: 0.7)
            PomoAnimations.animationViewPositionWithCompHandler(viewControllerView, toPosition: offScreen, duration: 0.7, completion: {
                viewControllerView.removeFromSuperview();
                viewController.removeFromParentViewController();
                viewController.dismissViewControllerAnimated(false, completion: nil)
                
                
            })
            }
            
        } else {
            viewControllerView.transform = scale;
            PomoAnimations.animationViewSize(viewControllerView, toSize: CGAffineTransformIdentity, duration: 0.7)
        }
    }
    
    
    //***** PUBLIC INTERFACE *******
    func updateDisplayModalView(storyBoardID : String) {
        addModalView(storyBoardID);
        self.taskIsHidden = true;
    }
    
    func displayDoneButton(storyBoardID : String) {
        addDoneView(storyBoardID)
        self.taskIsHidden = true
    }
    
    
    private func tintBackground () {
        let tempView = UIView(frame: parentViewController!.view.frame)
        tempView.backgroundColor = UIColor.blackColor()
        tempView.layer.opacity = 0.6
        tempView.layer.zPosition = 0  // make sure its at it's default level.
        parentViewController!.view.addSubview(tempView)
    }
    
    private func removeTint() {
        for view in parentViewController!.view.subviews {
            
            if view.layer.opacity == 0.6 {
                PomoAnimations.fadeView(view, duration: 0.3, completion: {
                    view.removeFromSuperview();
                })
            }
        }
    }
    
    
    private func addModalView(storyBoardID : String) {
        tintBackground();
        let mainViewFrameWidth = parentViewController!.view.frame.width;
        let mainViewFrameHeight = parentViewController!.view.frame.height;
        let sizeOfTaskView = CGSizeMake(mainViewFrameWidth / 1.2 , mainViewFrameHeight / 1.7)
        let viewController = instantiateViewController(storyBoardID)
        
        viewController.view.layer.zPosition = 1
        parentViewController!.addChildViewController(viewController)
        viewController.didMoveToParentViewController(parentViewController!)
        viewController.view.frame.size = sizeOfTaskView
        viewController.view.frame = CGRectMake((mainViewFrameWidth - sizeOfTaskView.width) / 2 , mainViewFrameHeight / 1.6 - sizeOfTaskView.height, sizeOfTaskView.width , sizeOfTaskView.height)
        parentViewController!.view.addSubview(viewController.view)
        animateModalWindow(viewController, isRemoval: false)
        
    }
    
    private func addDoneView(storyboardID : String) {
        let mainViewFrameWidth = parentViewController!.view.frame.width;
        let mainViewFrameHeight = parentViewController!.view.frame.height;
        
        let sizeOfDoneView = CGSizeMake(100,100)
        let doneViewController = instantiateViewController(storyboardID)
        doneViewController.view.layer.zPosition = 1;
        parentViewController?.addChildViewController(doneViewController)
        doneViewController.view.frame.size = sizeOfDoneView;
        doneViewController.view.frame = CGRectMake((mainViewFrameWidth - sizeOfDoneView.width) / 2 , mainViewFrameHeight / 1.38 - sizeOfDoneView.height, sizeOfDoneView.width , sizeOfDoneView.height)

        parentViewController!.view.addSubview(doneViewController.view)
        parentViewController!.view.bringSubviewToFront(doneViewController.view)
        
        animateModalWindow(doneViewController, isRemoval: false)
        
    }
    
    
    private func instantiateViewController(storyBoardID : String ) -> UIViewController {
        let viewController = parentViewController!.storyboard?.instantiateViewControllerWithIdentifier(storyBoardID) as UIViewController?
        
        if viewController is TaskViewController {
            (viewController as! TaskViewController).mainViewController = parentViewController;
            self.parentTaskViewController = (viewController as! TaskViewController)
            return viewController as! TaskViewController
        }
        if viewController is DoneViewController {
            return viewController as! DoneViewController
        }
        
        if viewController is SignUpViewController {
            return viewController as! SignUpViewController
        }
        
       
        return viewController!
    }
    
   
    func presentViewControllerFullScreen(storyboardID : String) {
        let controllerToPresent = instantiateViewController(storyboardID) as! StatsViewController;
        controllerToPresent.pomoSession =  parentViewController?.pomoSession
        
        presetViewController(controllerToPresent)
    }
    private func presetViewController(viewController : UIViewController) {
        parentViewController?.presentViewController(viewController, animated: true, completion: {
            
            
        })
    }
}

//
//  ViewController.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 5/17/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit
import Parse

class PomoTabBarController : UITabBarController, UITabBarControllerDelegate {
    
    var mainViewController : MainViewController?
    var settingsViewController : SettingsViewController?
    var fromView , toView : UIView?;
    var presenting = false;
    var slideAnimation : SlideAnimation?
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        slideAnimation = SlideAnimation();
        // Do any additional setup after loading the view, typically from a nib.
        mainViewController = findViewController().Main
        settingsViewController = findViewController().Settings
        
        //since these two controller will be talking to each other frequently, link them by reference.
        settingsViewController?.mainViewController = mainViewController;
        mainViewController?.settingsViewcontroller = settingsViewController;
        
        
        settingsViewController!.timerDelegate = mainViewController
        
        tabBarController?.transitioningDelegate = slideAnimation
        self.delegate = self;
        
        self.tabBar.tintColor = PomoUtils.lightBlueBar
    }
    
    
    
    func findViewController() -> (Main: MainViewController, Settings: SettingsViewController) {
        var mainViewController : MainViewController?
        var settingsViewController : SettingsViewController?
        
        for controller in self.viewControllers! {
            if controller is MainViewController {
                mainViewController = controller as? MainViewController
            }
            if controller is SettingsViewController {
                settingsViewController = controller as? SettingsViewController
            }
            
        }
        return (mainViewController!, settingsViewController!)
    }
    
 
    func tabBarController(tabBarController: UITabBarController, animationControllerForTransitionFromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
   
        self.slideAnimation!.presenting = !self.slideAnimation!.presenting
        
        return slideAnimation
    }
    

   
    func logoutClicked(logoutButton: UIButton) {
        let user = PFUser.currentUser()
        if user == nil {
            mainViewController?.isSignedIn = false;
            mainViewController?.usernameLabel.text = "not logged in"
            logoutButton.setTitle("Not Logged In", forState: .Normal)
            return
        }
        if  mainViewController!.isSignedIn {
            mainViewController?.usernameLabel.text = user!.username;
            logoutButton.setTitle("Logout", forState: .Normal)
        }
        
        
        
    }
    
   
    
  
 
    
}




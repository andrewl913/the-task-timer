//
//  DetailedStatsController.swift
//  Task Timer
//
//  Created by Andrew Lincoln on 7/11/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit
import Parse

class TimelineTableViewController: UITableViewController {
    var statsTableViewController :StatsTableViewController?
    var timeLineViewManager : TimelineViewManager?
    
    
  
    
    @IBOutlet weak var firstDetail: DetailViewCell!
    @IBOutlet weak var secondDetail: DetailViewCell!
    @IBOutlet weak var thirdDetail: DetailViewCell!
    @IBOutlet weak var fourthDetail: DetailViewCell!
    @IBOutlet weak var fifthDetail: DetailViewCell!
    
      var detailCells : [DetailViewCell] = []

    @IBOutlet weak var tableTitle: UILabel!
    
    
    @IBAction func backButtonPressed(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: {})

    }
    
    
    override func viewDidLoad() {
    
        super.viewDidLoad();
   
       
        print("tableviewloaded")
       
        //self.tableView.delegate = timeLineViewManager
        
    }
    override func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
        populateDetailCells();
        
         timeLineViewManager!.populateDataForDetailCells(detailCells)
        
        self.tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0), atScrollPosition: .Top, animated: true)
        
    }
    override func viewDidDisappear(animated: Bool) {
        
        resetCells();
        timeLineViewManager?.clearData()
        timeLineViewManager!.resetGraph();
        self.detailCells.removeAll(keepCapacity: false)
    }
    
    func populateDetailCells() {
        
        detailCells = [firstDetail, secondDetail, thirdDetail, fourthDetail, fifthDetail]
    }
    
    func resetCells(){
        for cell in self.detailCells {
            cell.progressBar.reset();
        
        }
    }
}

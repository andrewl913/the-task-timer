//
//  StatsViewController.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 7/4/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit
import Parse
class StatsViewController: UIViewController {
    

    
    var statsTableViewController : StatsTableViewController?
    
    @IBOutlet weak var usernameLabel: UILabel!
    var pomoSession : PomoSession?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        //pomoSession?.retrieveSessionDataFromParse();
        // pass off the pomoSession to the tableView
        statsTableViewController = findStatsTableViewController();
        statsTableViewController!.pomoSession = pomoSession
        
    
        displayTheUsernameTitle();
    }
    
    private func displayTheUsernameTitle() {
        let userNameText = PFUser.currentUser()!.username!.uppercaseString
        usernameLabel.text = "STATS FOR \(userNameText)"
    }
    
    override func viewWillAppear(animated: Bool) {
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func dismissButton(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func findStatsTableViewController() -> StatsTableViewController? {
        for viewController in self.childViewControllers {
            if viewController is StatsTableViewController {
                return viewController as? StatsTableViewController
            }
        }
        return nil
    }
    
    
    func hideTableView() {
        
        for viewController in self.childViewControllers {
            if let viewCont = viewController as? StatsTableViewController {
                viewCont.tableView.alpha = 0;
            }
        }
    }
}

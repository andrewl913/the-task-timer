//
//  SignUpViewController.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 6/25/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
  
  @IBOutlet weak var cancelButton: CancelSignUpButton!
  @IBOutlet weak var signInButton: SignInButton!
  @IBOutlet weak var registerButton: RegisterButton!
  @IBOutlet weak var usernameTextField: UsernameTextField!
  @IBOutlet weak var passwordTextField: PasswordTextField!
 
  var signInManager : SignInManager?
  var registrationManager : RegistrationManager?

  override func viewDidLoad() {
    
    super.viewDidLoad()
    PomoControllerUtils.cleanUpBackgroundColors(self)
    self.view.backgroundColor = UIColor.whiteColor()
    setUpTheView()
    
    //Initializers
    signInManager = SignInManager(signUpController: self);
    registrationManager = RegistrationManager(signUpController: self)
    passwordTextField.delegate = registrationManager
    usernameTextField.delegate = registrationManager
    
    
    /*
      There is an bug or prevention that will not allow buttons to respond via the interface builder so 
      in order to combat this blockade, make references to the sub classed buttons.
    */
    // ADD VIEW CONTROLLER REFERENCES 
    
    cancelButton.signUpViewController = self;
    signInButton.signUpViewController = self;
    registerButton.signUpViewController = self;
    
  
  }
  
  func cancelButtonPressed(sender: CancelSignUpButton) {
      dismissWindow()
  }
  
  
  func registerButtonPressed(sender : RegisterButton) {
    registrationManager?.registerUser();
   
  }

  
  func signInButtonPressed(sender : SignUpViewButton) {
    signInManager!.login();
  }
  
  func setUpTheView() {
    //self.view.layer.shadowColor = UIColor.blackColor().CGColor
    //self.view.layer.shadowOpacity = 0.2
    self.view.layer.cornerRadius = 12.0
    
  }
  
  
  func dismissWindow() {
    if self.parentViewController is MainViewController {
      let mainViewController = self.parentViewController as! MainViewController
      mainViewController.modalVCManager?.taskIsHidden = false;
    }
  }
  
  
  
  
  
}

//
//  DoneViewController.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 7/7/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class DoneViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getDoneButton() -> DoneButton? {
        if view is DoneButton {
            print("Found done button")
            return view as? DoneButton
            
        }
        return nil
    }
    

}

//
//  StopWarningController.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 6/22/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class StopWarningController: NSObject {
    
    var alertViewController = UIAlertController(title: "Are you sure?", message: "This will remove all your tasks end and save your current session!", preferredStyle: UIAlertControllerStyle.Alert);
    var mainViewController : MainViewController?
    var viewController : UIViewController?
    var continueAction : UIAlertAction?
    var stopAction : UIAlertAction?
    
    init(viewController : UIViewController) {
        let subview = self.alertViewController.view.subviews.first as UIView!
        let alertContentView = subview.subviews.first as UIView?
        self.viewController = viewController;
        alertContentView!.backgroundColor = UIColor(red: 219/255, green: 218/255, blue: 218/255, alpha: 45/100)
        alertContentView!.layer.cornerRadius = 5;
        
        
        stopAction = UIAlertAction(title: "End My Session", style: UIAlertActionStyle.Default, handler: { action in
            //make sure the pomosession is the controllers event listener be we call the stop method
            if let viewController = viewController as? MainViewController {
                
                if(viewController.timer!.isActive) {
                    // this is a case where we are resetting from the preferences
                   
                }
                
                viewController.startButton.breakStarted = false;
                viewController.startButton.resetStrokePosition()
                viewController.timer?.tomatoAnimator?.resetTomatoes()
                viewController.stopTimer()
                viewController.rebuildTheTimer()

                if viewController.timer!.isButtonReset() {
                    viewController.animateTimerLabel() // animate the timer label to let the user know the timer is at it's max threshold
                    
                }
                viewController.pomoSession!.resetData();
                viewController.timer?.reset()
            }
        
        })
        
        continueAction = UIAlertAction(title: "Nevermind", style: UIAlertActionStyle.Cancel, handler: { action in
            return;
        })
        
        alertViewController.addAction(continueAction!)
        alertViewController.addAction(stopAction!)
        self.alertViewController.view.tintColor = PomoUtils.tomatoRedColor
        
    }
    
    func showAndActOnWarningMessage() {
        self.viewController?.presentViewController(alertViewController, animated: true, completion: {})
        
    }
}

//
//  PomoSession.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 6/27/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//
//

import UIKit
import Parse


class PomoSession: NSObject, PomoEventListenable, NSCoding , NSCodable {
    
    //The purpose of this class is to keep a record of when the user begins or stops a session
    typealias T = PomoSession
    
    var Id = 0;
    var taskList : [String] = [];
    var user : PFUser?

    
    var pomoNotificationListener : PomoNotificationListener?
    
    var lastPomoSession : PomoSession?
    
    var sessionCanBegin = true;
    var sessionBegan = false;
    var sessionEnded = false;
    
    
    // *** SESSION VARIABLES ********
    var stopCount : Int  = 0
    var sessionCount : Int = 0
    
    
    // *** TASK VARIABLES *******
    var tasksMadeCount : Int = 0
    var tasksFinishedCount : Int = 0;
    var percentageOfTasksComplete : Float = 0.0;
    var allTimeTasks : Int = 0
    
    
    //*** TOMATO VARIABLES ********
    var numberOfTomatoes : Int = 0;
    
    //*** OVERALL LIFE ACTIVTIVY VARIABLES *********
    
    var longestSession : Int = 0;
    var overallSessionTime : Int = 0;
    
    
    //**** PFOBJECTS **********
    
    var sessionObject,
    lifeActivityObject : PFObject?
    
    var pullingObject : PFObject?
    var sessionQuery : PFQuery?
    
    
    
   
    
    
    
    override init() {
        //***** INIT VALUES
        self.stopCount = 0;
        self.sessionCount = 0 //change to fetch the session count from parse
        self.longestSession = 0 // fetch this data from parse
        self.overallSessionTime = 0 //compound this from the data fetched from parse
        
        self.tasksMadeCount = 0;
        self.tasksFinishedCount = 0;
        self.percentageOfTasksComplete = 0.0;
        
        super.init();
        
        //**** INIT OBJECTS
        user = PFUser.currentUser();
        pomoNotificationListener = PomoNotificationListener();
     
        
    }
    
     //*** NS CODING *****
    
    required init?(coder aDecoder: NSCoder) {
        
        stopCount = aDecoder.decodeIntegerForKey("stopCount")
        sessionCount = aDecoder.decodeIntegerForKey("sessionCount")
        tasksMadeCount = aDecoder.decodeIntegerForKey("tasksMadeCount")
        tasksFinishedCount = aDecoder.decodeIntegerForKey("tasksFinishedCount")
        percentageOfTasksComplete = aDecoder.decodeFloatForKey("percentageOfTasksComplete")
        allTimeTasks = aDecoder.decodeIntegerForKey("allTimeTasks")
        numberOfTomatoes = aDecoder.decodeIntegerForKey("numberOfTomatoes")
        
        
        //NOTE: Longest session and overall session will be stored in the parse backend
        
        
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeInteger(self.stopCount, forKey: "stopCount")
        aCoder.encodeInteger(self.sessionCount, forKey: "sessionCount")
        aCoder.encodeInteger(self.tasksMadeCount, forKey: "tasksMadeCount")
        aCoder.encodeInteger(self.tasksFinishedCount, forKey: "tasksFinishedCount")
        aCoder.encodeFloat(self.percentageOfTasksComplete, forKey: "percentageOfTasksComplete")
        aCoder.encodeInteger(self.allTimeTasks, forKey: "allTimeTasks")
        aCoder.encodeInteger(self.numberOfTomatoes, forKey: "numberOfTomatoes")
    }
    
    
    
    
    func save() {
        let archiver = NSKeyedArchiver.archivedDataWithRootObject(self)
        NSUserDefaults.standardUserDefaults().setObject(archiver, forKey: "pomoSession")
        saveStatsToParse();
        
        
    }
    
    func saveStatsToParse () {
        
    
        sessionObject = PFObject(className: "PomoSession")
        
        if let sessionObject = self.sessionObject {
            
            sessionObject["user"] = PFUser.currentUser()!
            
            print(" Stop Count \(self.stopCount) ")
            sessionObject["stopCount"] = self.stopCount;
            sessionObject["sessionCount"] = self.sessionCount
            sessionObject["tasksMadeCount"] = self.tasksMadeCount
            sessionObject["tasksFinishedCount"] = self.tasksFinishedCount
            sessionObject["percentageOfTasksComplete"] = self.percentageOfTasksComplete
            sessionObject["allTimeTasks"] = self.allTimeTasks
            sessionObject["numberOfTomatoes"] = self.numberOfTomatoes
            sessionObject["overallSessionTime"] = self.overallSessionTime;
            saveData(sessionObject)
            
           
        }
        
    }
    
    
    func clear() {
        NSUserDefaults.standardUserDefaults().removeObjectForKey("pomoSession")
    }
    
    
    static func loadData() -> PomoSession? {
        
    

        if let data = NSUserDefaults.standardUserDefaults().objectForKey("pomoSession") as? NSData {
            return NSKeyedUnarchiver.unarchiveObjectWithData(data) as? PomoSession
        }
        print("Couldn't find the Pomo Session Data.. did you save it?");
        return nil
    }
    
    
    
 
    
    /// STATES OF THE SESSION.
    
    func start() {
        // session has started when this method is called
        // A session can only begin once so ignore any other calls to this method.
        sessionBegan = true;
        if(sessionCanBegin){
            
            pomoNotificationListener!.start();
            
            if ( PFUser.currentUser() != nil) {
             print("Session starting")
            }
            sessionCanBegin = false;
        }
        
        
    }

    
    func stop() {
        stopCount++;
        pomoNotificationListener!.stop()
    }
    
    func end() { // TODO : rename to ended();
        pomoNotificationListener!.end();
        sessionEnded = true;
        sessionCanBegin = true;
        sessionBegan = false;
        sessionCount++;
        print("sessionEnded")
        //compileSessionData()
        resetData();
    }
    
    
    
    func longBreakStarted() {
       
    }
    
    func breakStarted() {
    
        numberOfTomatoes++;
        pomoNotificationListener!.breakStarted()
    }
    
    func timeIncremented() {
        overallSessionTime++;
    }
    
    
    
    func taskAdded(outOf: Int) {
        tasksMadeCount = outOf;
    }
    func taskFinished(outOf: Int) {
        tasksFinishedCount++;
    }
    
    func resetData() {
        self.save()
        discardTasks();
        transferStatsToLastSession()
        stopCount = 0;

        tasksMadeCount = 0;
        tasksFinishedCount  = 0;
        percentageOfTasksComplete = 0.0;
        allTimeTasks  = 0
        
        numberOfTomatoes  = 0;
        
        longestSession = 0;
        overallSessionTime  = 0;
    }
    
    
    func getLongestSession() -> PFObject? {
        let query = PFQuery(className: "PomoSession")
        query.whereKey("user", equalTo: PFUser.currentUser()!)
        query.orderByDescending("overallSessionTime")
        // return the longest session.
        return query.getFirstObject();
    }
    
    func getBiggestValueObject(key : String ) -> PFObject? {
        let query = PFQuery(className: "PomoSession")
        query.whereKey("user", equalTo: PFUser.currentUser()!)
        query.orderByDescending(key)
        return query.getFirstObject()
    }
    
    
    func fetchDataForUser(user: PFUser!) -> PFObject? {
        let sessionQuery = PFQuery(className: "PomoSession")
        sessionQuery.whereKey("user", equalTo: user)
        sessionQuery.orderByDescending("createdAt")
        
       
        return getObjectForQuery(sessionQuery, index: 0 );
        
    }
    
    
    func getObjectForQuery(query : PFQuery, index : Int) -> PFObject? {
   
        if let objects = query.findObjects() as? [PFObject] {
            if objects.count > 0 {
                print(objects[index]);
            return objects[index]
            }
            
        }
       
        return nil
    
    }
    
}

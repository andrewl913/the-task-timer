//
//  TaskViewController.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 6/24/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit


class TaskViewController: UIViewController {
    
    
    var mainViewController : MainViewController?
    var pomoTaskManager : PomoTaskManager?
    var modalVCManager : ModalVCManager?
    var doneButton : DoneButton?
    
    @IBOutlet weak var taskTable : TaskTable!
    
    @IBOutlet weak var addButton: UIButton!
    
    @IBAction func backButtonPressed(sender: UIButton) {
        if self.parentViewController is MainViewController {
            let mainViewController = self.parentViewController as! MainViewController
            
            print("found main view!\n", terminator: "")
            mainViewController.modalVCManager!.taskIsHidden = false;
            //self.pomoTaskManager.pomoSession?.taskList = self.pomoTaskManager.taskList
            
            //remove the last element from the saved PomoTaskStorageManager
          
        }
        
        //self.dismissViewControllerAnimated(true, completion: nil)            //formally dismiss the modal task manager
        //self.view.removeFromSuperview();               // informally remove the view
        // self.removeFromParentViewController();       // get it off the mainview
    }
    var alreadyRemoved = false
    override func viewDidLoad() {
        super.viewDidLoad()
        print("View Loaded\n", terminator: "")
      
        if let mainViewCont = mainViewController {
            print(mainViewCont.taskList)
            
            self.pomoTaskManager = PomoTaskManager(taskViewController: self, taskList: mainViewCont.taskList, mainViewController : mainViewCont)
            
            modalVCManager = ModalVCManager(viewController: mainViewCont)
           
        }
 
        self.view.backgroundColor = PomoUtils.taskPurpleColor
        self.view.layer.backgroundColor = PomoUtils.taskPurpleColor.CGColor
        setUpTheTaskView()
        assignDelegates();
        addButton.addTarget(pomoTaskManager, action: Selector("addButtonPressed"), forControlEvents: UIControlEvents.TouchUpInside)

        if let pomoStorage = PomoTaskStorageManager.loadSavedManager() {
            if pomoStorage.taskList.count >= 8 {
                //pomoStorage.taskList.removeLast();
                mainViewController?.noCellsLeft = true;
                pomoTaskManager!.noCellsLeft = mainViewController!.noCellsLeft
                pomoStorage.save()
                
            }
            
        }
    }
    
    override func viewDidAppear(animated: Bool) {
    
      
        pomoTaskManager?.moveToBottomOfTableView()
        if let pomoStorage = PomoTaskStorageManager.loadSavedManager() {
            for task in pomoStorage.taskList {
                if task.isFinished {
                    if(!pomoTaskManager!.displayedAlready) {
                        mainViewController!.modalVCManager?.displayDoneButton("DoneViewController")
                       
                        pomoTaskManager?.displayedAlready = true;
                    }
                }
            }
        }
        doneButton = self.modalVCManager?.getDoneButton()
        doneButton?.doneButtonDelegate = pomoTaskManager;
        pomoTaskManager?.moveToBottomOfTableView()

    }
    
    func setUpTheTaskView() {
        
        //self.view.layer.shadowColor = UIColor.blackColor().CGColor
        //self.view.layer.shadowOpacity = 16.0;
        self.view.layer.cornerRadius = 12.0
        
    }
    
    func assignMainViewController() {
        if let mvController = self.parentViewController as? MainViewController {
            self.mainViewController = mvController;
        }
    }
    
    private func getTableView () -> UITableView {
        for view in self.view.subviews {
            if view is UITableView {
                return view as! UITableView
            } else {
                print("Couldn't find the UITableView \n", terminator: "")
            }
        }
        return UITableView();
    }
    
    private func assignDelegates() {
        if taskTable != nil {
            taskTable.delegate = pomoTaskManager
            taskTable.dataSource = pomoTaskManager
        }
    }
    
}

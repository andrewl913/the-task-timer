//
//  PomoTaskManagerExtension.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 7/7/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit
import Parse

extension PomoTaskManager: UITableViewDelegate, UITableViewDataSource ,  UITextFieldDelegate , DoneButtonDelegate {
    
     //************ TABLE VIEW DELEGATE ****************
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        taskCell = TaskCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "TaskCell", taskManager: self)
        
        taskCell!.textLabel?.text = taskList[indexPath.row].taskText
       
        // the text is an empty screen, render a textfield on the cell
        if(taskCell!.textLabel!.text == "") {
            if(taskList.count <= 8) {
                taskCell!.showTextField()
            } else {
                removeTheBuffer()
            }
 
        } else {
           
            taskCell!.hideTextField();
        }
       // print("index: \(indexPath.row)\n")
        
        let finishedButton = taskCell?.finishedButton
        
        // if the tag hasn't been assigned
        if(finishedButton != nil) {
            
            finishedButton?.isFinished = taskList[indexPath.row ].isFinished
            //assign the tag of the button to our current index in the table view
            finishedButton?.tag = indexPath.row
            if(finishedButton!.isFinished) {
                //remove the cell here
                print("Button \(finishedButton!.tag): Task is finished\n", terminator: "")
            }
        }
        
        finishedButton?.setTitleColor(taskList[indexPath.row].color, forState: .Normal)
        
        
        return taskCell!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskList.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
       
     
            PomoAnimations.animateCell(cell, duration: 0.4)
        
    }
    
    
    //************ TEXT FIELD DELEGATE ****************
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        addButtonPressed()
        textField.resignFirstResponder()
        return true;
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        textField.addTarget(self, action: Selector("taskTextChanged:"), forControlEvents: UIControlEvents.EditingChanged)
    }
    
    func taskTextChanged(textField : UITextField) {
        taskTextFieldText = textField.text!
        
        // print(taskTextFieldText)
    }
    
    // set the max character length
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if ( range.length + range.location > textField.text!.characters.count) {
            return false;
        }
        print(textField.text, terminator: "")
        let newLength = textField.text!.characters.count + string.characters.count - range.length
        return newLength <= 28
    }
    
    
    //****** DONE BUTTON PRESSED
    
    func doneButtonPressed(button: DoneButton) {
        print("Done button! YAY")
        //iterate through the pomostoragemanager
        let taskTable = self.taskViewController?.taskTable;
      
        
        if let pomoStorageManager = PomoTaskStorageManager.loadSavedManager() {
           taskList = pomoStorageManager.taskList
            
            for ( var i = 0 ; i < taskList.count  ; i++) {
                
    
                if(taskList[i].isFinished) {
                    
                    pomoListener?.taskFinished(pomoStorageManager.taskList.count)
                    taskList.removeAtIndex(i)
                     print("*******Deleting current index: \(i) out of : \(taskList.count)")
                    //retrace steps to get back to elements we may have missed. 
                  
                    taskTable?.scrollToRowAtIndexPath(NSIndexPath(forRow: 0 , inSection: 0), atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
            
                    i--;
                  
                    taskTable!.setContentOffset(CGPointMake(0, taskTable!.contentSize.height - taskTable!.frame.size.height), animated: false)

                }
            }
            removeLast(); //removes the text tabe
            taskTable?.reloadData()
            pomoStorageManager.taskList = taskList
            pomoStorageManager.save()
          
           // taskTable?.reloadData()
            checkFinishedState()
            
        }
        
    }

}

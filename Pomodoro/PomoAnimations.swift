//
//  PomoAnimations.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 6/25/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class PomoAnimations: NSObject {
    
    static func animationButtonTextColor(button : UIButton, toColor : UIColor, duration : NSTimeInterval) {
        UIView.transitionWithView(button, duration: duration, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
            button.setTitleColor(toColor, forState: .Normal)
            
            }, completion: nil)
    }
    
    static func animationButtonSize( button : UIButton, toSize : CGAffineTransform, duration : NSTimeInterval) {
        
        UIView.animateWithDuration(duration, animations: {
            
            button.transform = toSize;
            }, completion: { done in })
    }
    static func animationViewSize ( view : UIView, toSize : CGAffineTransform, duration : NSTimeInterval) {
        UIView.animateWithDuration(duration, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1.4, options: .CurveEaseIn, animations: {
            view.transform = toSize
            
            }, completion: nil)
    }
    
    
    static func animationViewPositionWithCompHandler ( view : UIView, toPosition : CGAffineTransform, duration : NSTimeInterval, completion : () -> () ) {
        
        UIView.animateWithDuration(duration, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1.4, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            
            view.transform = toPosition;
            
            }, completion: { done in
            completion();
        })
    
    
    }
    
    static func animationViewAngle ( view : UIView, angle: CGAffineTransform, duration : NSTimeInterval  ) {
        view.layer.anchorPoint = CGPointMake(1,1)        //set the anchor point to the bottom left of the view
        view.center = CGPointMake(CGRectGetWidth(view.bounds), CGRectGetHeight(view.bounds));
        UIView.animateWithDuration(duration, animations: {
            view.transform = angle
        })
    }
    
    
    static func fadeView( view : UIView, duration : NSTimeInterval , completion : () -> ()) {
        
        UIView.animateWithDuration(duration, animations: {
            view.alpha = view.alpha >= 0.01 ? 0 : 1;     //view is 100% we change it to zero, if it isn't we change to 1
            
            }) { done in
                
                completion();
        }
    }
    
    
    static func shakeView( view : UIView, duration : NSTimeInterval) {
       // var shakeCounter : Int = 2;
        let distance : CGFloat = 10
        let leftOffset = -view.frame.minX + distance
        let rightOffset = view.frame.minX - distance
        let leftOffsetTransform = CGAffineTransformMakeTranslation(leftOffset, 0)
        let rightOffsetTransform = CGAffineTransformMakeTranslation(rightOffset, 0)
        let identity = CGAffineTransformIdentity;
        
        UIView.animateWithDuration( NSTimeInterval(duration * 1/3) , delay: 0, usingSpringWithDamping: 0.1 , initialSpringVelocity: 6, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            view.transform = leftOffsetTransform;
            
            }, completion: { done in
                //exponentially bring the shake to a halt
                UIView.animateWithDuration(NSTimeInterval(duration * 1/6), delay: 0, usingSpringWithDamping: 1.4, initialSpringVelocity: 3, options: .CurveEaseOut, animations: {
                    view.transform = rightOffsetTransform
                    
                    }, completion: {done in
                        UIView.animateWithDuration(duration * 1/12, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 4, options: .CurveEaseOut, animations: {}, completion: { done in
                            view.transform = identity
                            
                        })
                        
                })
        })
    }
    
    static func rotateView(view : UIView, duration : NSTimeInterval, angle : CGFloat) {
        
        let angleToRotate  = CGAffineTransformMakeRotation(angle)
        
        UIView.animateWithDuration(duration, animations: {
            view.transform = angleToRotate
            
        })
    }
    
    static func resetToIdentity(view : UIView, duration : NSTimeInterval) {
        UIView.animateWithDuration(duration, animations: {
            view.transform = CGAffineTransformIdentity
            
        })
    }
    
    static func animateTimelineCells(cell : UITableViewCell , duration : NSTimeInterval, alternator : Int) {
        
        let angleOffset = alternator % 2 == 0 ? CGAffineTransformMakeRotation(CGFloat(M_1_PI/6)) : CGAffineTransformMakeRotation(CGFloat(-M_1_PI/6))
        cell.alpha = 0.5;
        cell.transform = angleOffset;
        UIView.animateWithDuration(duration, animations: {
            cell.alpha = 1;
            
           cell.transform = CGAffineTransformIdentity
            
        
        })
        
    }
    
    static func animateCell(cell :UITableViewCell, duration : NSTimeInterval ) {
        let vertOffset = CGAffineTransformMakeTranslation(0, 20)
        cell.transform = vertOffset
        
        UIView.beginAnimations("Translation", context: nil)
        UIView.setAnimationDuration(duration)
        cell.transform = CGAffineTransformIdentity
        
        UIView.commitAnimations()
        
        
        
    }
    
    static func animationCalendarButton(button : UIButton,  duration : NSTimeInterval, size : CGAffineTransform) {
        
 
        UIView.animateWithDuration(duration, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1, options: .CurveEaseIn, animations: {
            
            button.transform = size;
            
            }, completion: { done in
        
        
        })
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    static func pushTaskButtonOffscreen(button: UIButton) {
        button.layer.anchorPoint = button.center
        let rightOffset = CGAffineTransformMakeTranslation(button.superview!.bounds.width, 0)
        
        let rotation = CGAffineTransformMakeRotation(CGFloat(M_1_PI)) // rotate 180 degrees;
        UIView.animateWithDuration(0.3, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 2, options: .CurveEaseIn, animations: {
            
            
            button.transform = rotation
            button.transform = rightOffset;
            
        }, completion: nil)
        
    }
    
    
    
    
}

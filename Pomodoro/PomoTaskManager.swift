  //
  //  PomoTaskManager.swift
  //  Pomodoro
  //
  //  Created by Andrew Lincoln on 6/24/15.
  //  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
  //
  
  import UIKit
  import Parse
  
  

  
  class PomoTaskManager: NSObject {
    
    var coder : NSCoder?

    var taskList : [PomoTask] = [];
    var loadedTask : PomoTask?
    var doneButton : DoneButton?
    var mainViewController : MainViewController?
    var doneViewController : DoneViewController?
    var taskLimit : Int?
    
    var pomoListener : PomoEventListenable?
    private var storedTasks : [String] = [];
    
    static let sharedTaskManager = PomoTaskManager();
    
    var pomoSession : PomoSession?
    
    var taskViewController : TaskViewController?
    
    var taskCell : TaskCell?

    var taskTextFieldText : String = "";
  
    var lastTask : String {
        get {
    
            if(self.taskList.last?.taskText == nil){
                return "broken"
            }
            return self.taskList.last!.taskText
        }
    }
    
    init (taskViewController : TaskViewController, taskList: [PomoTask], mainViewController : MainViewController) {
        super.init();
        taskLimit = 7;
        
        self.taskViewController = taskViewController
        self.mainViewController = mainViewController;
        self.doneButton = taskViewController.modalVCManager?.getDoneButton();
        self.doneButton?.addTarget(self, action: "doneButtonPressed:", forControlEvents: .TouchUpInside)
        self.pomoSession = mainViewController.pomoSession
        self.pomoListener = pomoSession
        updateStorageOnLoad();
        
        
        
    }
    
    override init() {
        
    }
    
    func addTask(task : String, isFinished : Bool, color : UIColor) {
        self.taskList.append(PomoTask(taskText: task , isFinished: isFinished, color: color));
        pomoListener?.taskAdded(taskList.count)
        let pomoStorageManager = PomoTaskStorageManager()
        pomoStorageManager.taskList = self.taskList
        pomoStorageManager.save()
        for task in self.taskList {
            task.save()
            
        }
      
    }
    
    func retrieveTasksFromParse() -> [String] {
        let taskQuery = PFQuery(className: "PomoSession")
        var pfObject : PFObject?
        
        taskQuery.whereKey("userForSession", equalTo: PFUser.currentUser()!)
        
        pfObject = taskQuery.getFirstObject();
        
        let strings : [String] = [];
        
        
        if let string = pfObject!["tasks"] as? [NSString] {
            for string in string {
                print(string, terminator: "");
            }
            return  string as! [String]
        }
        print(pfObject!["tasks"]!, terminator: "");
        return strings
    }
    

    //********* ADD BUTTON LISTENER *************
    //note: this listener is being added from the TaskViewController
    
    // we can remove the cell
    var canRemove = true;
    
    //no more tasks can be filled with tasks
    var noCellsLeft = false;
    
    var isFocused = false
    var offSet = 0
    func addButtonPressed() {
        let taskTable = self.taskViewController?.taskTable;
        
        if let task = taskCell {
            // make the taskfield the first responder will clicked to prevent the tableview cell from scrolling up
            task.taskTextField?.becomeFirstResponder()
        }
        
        if taskList.count > taskLimit {
            print(noCellsLeft, terminator: "");
         
           //removeTheBuffer()
            noCellsLeft = true;
            print(taskLimit, terminator: "")
        } else {
            noCellsLeft = false
        }
        
        if noCellsLeft {
            
            //removeTheBuffer()
            return;
        }
        
        //constant updates of the taskTextField will allow this condition to execute
        appendTheTaskToTableView()
        
        // if the last element was empty... we are waiting for the user to add a task, don't do anything else until the task is filled
        if(lastTask == "") {
           
            if let cell = taskCell {
                if let textField = cell.taskTextField {
                    if(textField.isFirstResponder()) {
                        //if the text field is the first responder, exit the function to prevent the tableview from scrolling upwards
                        return;
                    }
                }
            }
           
            taskTable?.scrollToRowAtIndexPath(NSIndexPath(forRow: taskList.endIndex - 1 , inSection: 0), atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
            //taskCell?.taskTextField?.becomeFirstResponder()
            return;
        }
        // after the task is added, append an empty string to present a new text field for the user to enter in another task
        appendAnotherTextField()
    }
    
    // ***** ADD TASK HELPER METHODS
    
    
    //conditionally check the task table to see if the there is a text field in the buffer, if not
    // we can add a task.
    func appendTheTaskToTableView() {
        
        if taskTextFieldText != "" {
            addTask(taskTextFieldText , isFinished: false, color: PomoUtils.tomatoRedColor)
            removeTextFields()         // remove any all text fields from the buffer
            taskTextFieldText = "";    //clear the buffer
        }
    }
    
    func removeLast()  {
        addTask(" ", isFinished: false, color: PomoUtils.tomatoRedColor)
        taskList.removeLast()
        removeTextFields()
        
       
    }
    func appendAnotherTextField() {
        let taskTable = self.taskViewController!.taskTable!;
        addTask("" , isFinished: false, color: PomoUtils.tomatoRedColor)
        isFocused = true;
        
        //update the data and scroll if needed
       taskTable.reloadData()
       taskTable.setContentOffset(CGPointMake(0, taskTable.contentSize.height - taskTable.frame.size.height), animated: false)
    }
    
    
    func moveToBottomOfTableView() {
        let taskTable = self.taskViewController!.taskTable!;
                //update the data and scroll if needed
       taskTable.reloadData()
        taskTable.setContentOffset(CGPointMake(0, taskTable.contentSize.height - taskTable.frame.size.height), animated: false)
       // taskTable.scrollToRowAtIndexPath(NSIndexPath(forRow: taskList.endIndex - 1 , inSection: 0), atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
        

    }
    func removeTheBuffer() {
        let taskTable = self.taskViewController!.taskTable
        if(noCellsLeft && canRemove) {
            taskCell?.taskTextField?.placeholder = ""
            //remove the last cell that is acting as the buffer.
            let path = NSIndexPath(forRow: taskList.endIndex - 1, inSection: 0)
            let array : NSArray = [path]
            
            updateTaskList()
            removeTextFields()
            
            taskCell?.taskTextField?.resignFirstResponder();
            
            self.taskViewController!.taskTable!.deleteRowsAtIndexPaths(array as! [NSIndexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        
            
             taskTable.reloadData()
            canRemove = false;
            
        }

    }
    
    func updateTaskList() {
        if let pomoStorage = PomoTaskStorageManager.loadSavedManager() {
                pomoStorage.taskList.removeLast();
                pomoStorage.save()
 
        }

    }
    func removeTextFields() {
        
        for(var i = 0 ; i < taskList.count ; i++) {
            //there is a text field at the bottom location, so remove it for the next iteration
            if(taskList[i].taskText == "") {
                taskList.removeAtIndex(i);
            }
        }
    }
    
    func finishedButtonClicked(sender : FinishButton) {
        var color = PomoUtils.tomatoRedColor;
        if (!sender.isFinished) {
            sender.isFinished = true;
            color = PomoUtils.startGreenColor
        
        } else {
            sender.isFinished = false;
            color = PomoUtils.tomatoRedColor;
        }
        updateFinishedState(color , sender: sender);
        checkFinishedState()
      //  print("Button Clicked Tag : \(sender.tag)\n");
        
    }
    

    func updateFinishedState(color : UIColor, sender : FinishButton) {
        if let pomoTaskStorage = PomoTaskStorageManager.loadSavedManager() {
            
                if pomoTaskStorage.taskList[sender.tag].isFinished {
                print("Button Tag \(sender.tag) is not finished anymore")
                pomoTaskStorage.taskList[sender.tag].isFinished = false;
                pomoTaskStorage.taskList[sender.tag].color = PomoUtils.tomatoRedColor;
                pomoTaskStorage.save();
                self.taskList = pomoTaskStorage.taskList
                return;
                }
            // save to the backend
            pomoTaskStorage.taskList[sender.tag].color = color;
            pomoTaskStorage.taskList[sender.tag].isFinished = sender.isFinished
            
            
             print("finished: Button Tag \(sender.tag) \(pomoTaskStorage.taskList[sender.tag].isFinished) <- SHOULD BE TRUE!")

            self.taskList = pomoTaskStorage.taskList

            pomoTaskStorage.save()
        }

    }
    
    
    
    
    var displayedAlready = false;
    func checkFinishedState() {
        var allTasksFinished : Bool = false;
        if let pomoStorageManager = PomoTaskStorageManager.loadSavedManager() {
            for task in pomoStorageManager.taskList {
                if task.isFinished {
                    allTasksFinished = task.isFinished
                }
            }
            
            if(!allTasksFinished) {
                mainViewController?.modalVCManager?.removeDoneButton()
                displayedAlready = false

            } else {
                if(!displayedAlready) {
                mainViewController?.modalVCManager?.displayDoneButton("DoneViewController")
                    
                    doneButton = taskViewController!.modalVCManager?.getDoneButton()
                    doneButton?.doneButtonDelegate = self;
                    displayedAlready  = true;
                }
            }
           
        } else {
            
        }

    }
    
    func updateStorageOnLoad() {
        if let pomoStorageManager = PomoTaskStorageManager.loadSavedManager() {
            print("loading the tasks", terminator: "");
            
            self.taskList = pomoStorageManager.taskList
        } else {
            print("couldn't find the tasks\n", terminator: "")
        }
    }
    
  }

//
//  SignInManager.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 7/3/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit
import Parse

class SignInManager: NSObject {
// purpose of this class is to manage the sign in logic
  
  var user : PFUser?
  var signUpController : SignUpViewController;
  var registrationManager : RegistrationManager?
  var message = ""
 
  
  init(signUpController : SignUpViewController) {
   
    self.signUpController = signUpController;
  }
  
  func login()  {
    
    PFUser.logInWithUsernameInBackground(signUpController.usernameTextField.text! , password:signUpController.passwordTextField.text!) { user, error in
      
      if(user != nil) {
        self.user = user
        (self.signUpController.parentViewController as! MainViewController).isSignedIn = true;
        (self.signUpController.parentViewController as! MainViewController).usernameLabel.text = user!.username
        (self.signUpController.parentViewController as! MainViewController).usernameLabel.updateConstraintsIfNeeded()
        
        self.signUpController.dismissWindow()
      } else {
        //rattle the view 
        PomoAnimations.shakeView(self.signUpController.view, duration: 0.5)
        self.signUpController.usernameTextField.text = ""
        self.signUpController.passwordTextField.text = ""
        self.signUpController.usernameTextField.placeholder = "Invalid username or password"
        self.signUpController.passwordTextField.placeholder = "Try again.."
        
      }
    }
  }
}

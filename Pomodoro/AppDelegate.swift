//
//  AppDelegate.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 5/17/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit
import Parse

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, TimerFireDelegate {

  var window: UIWindow?
  var timer : NSTimer?;
  var pomoTimer : PomoTimer?


  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    // Override point for customization after application launch.
    
    
    application.idleTimerDisabled = true;
    
    application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil))
    
    application.registerForRemoteNotifications();
    
    application.currentUserNotificationSettings();
  
    Parse.setApplicationId("omitted",
      clientKey: "omitted")
    
    let registeredUser = PFUser.currentUser();
    
  
    
    if(registeredUser != nil) {
     // let storyboard = UIStoryboard(name: "Main", bundle: nil)
      
      //let mainViewController = storyboard.instantiateViewControllerWithIdentifier("MainViewController") as! MainViewController
      
      //mainViewController.signInButton.loginPressed = false // don't show the login button, convert it to the stats button when the view controller initially loads
    
      
      print("Registered user : \(registeredUser?.username)");
      
      
    }
    return true
  }
    
    
   

  func applicationWillResignActive(application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
  }

  func applicationDidEnterBackground(application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    let app = UIApplication.sharedApplication();
    window?.backgroundColor = UIColor.whiteColor()
    var bgTask : UIBackgroundTaskIdentifier
    bgTask = 0;
    
    bgTask = app.beginBackgroundTaskWithExpirationHandler({
        
        print("time remaining: \(application.backgroundTimeRemaining)")
      app.endBackgroundTask(bgTask)
      bgTask = UIBackgroundTaskInvalid
    })
    
    //dispatch asynchronously to the background thread to allow the timer to continue firing while the application is in the background
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
      
      if let timer = self.timer {
        //re initialize the timer with a firing rate of 1 second to allow the run loop to effectively keep the timer alive
        if !self.pomoTimer!.isActive {
          
          return;    //timer was stopped so don't start another one or else the timer will execute.
        }
        self.timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self.pomoTimer!, selector: "decrementOperation", userInfo: nil, repeats: true)
        NSRunLoop.currentRunLoop().addTimer(timer, forMode: NSDefaultRunLoopMode)               //add it the timer to runloop instance
        NSRunLoop.currentRunLoop().run();                                                       // run the timer
      }
    
    })
   
   
    
  }

  func applicationWillEnterForeground(application: UIApplication) {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
  }

  func applicationDidBecomeActive(application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    if let timer = self.timer {
      if pomoTimer!.isActive {
        //if the timer is active, invalidate this one to avoid an issue with duplicate timers.
        timer.invalidate();
        
      }
    }
  }

  func applicationWillTerminate(application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
  }
  
  func timerToFire(timer: NSTimer, pomoTimer: PomoTimer) {
    self.timer = timer;
    self.pomoTimer = pomoTimer;
  }

}


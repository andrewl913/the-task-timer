//
//  StartButton.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 5/25/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//


import UIKit


class StartButton : UIButton {
    
    var completion : (()->())? // This is a function reference. Since swift will not let one exist without an initializer present
    
    var position : CGFloat = 0;
    
   
    
    var currentPosition : CGFloat = 0;
    var deltaTime : NSTimer?
    var deltaPosition : CGFloat = 0.0
    
    
    var showStats = false  {
        didSet {
            setNeedsDisplay();
        }
    }
    var breakStarted  = false {
        didSet {
            setNeedsDisplay()
        }
    }
    var longBreakStarted = false {
        didSet {
            setNeedsDisplay()
        }
    }
    var resetPosition : CGFloat = -270
    var markedPosition : CGFloat = 0
    
    var progressAngle : CGFloat = -270.0 {
        didSet {
            setNeedsDisplay()
            
        }
    }
    
    override func drawRect(rect: CGRect) {
        self.backgroundColor = UIColor.clearColor();
        PomoUtils.drawStartStopToggleButton(frame: rect, progressAngle: progressAngle, breakStarted: breakStarted, longBreakStarted: longBreakStarted, showStat: showStats)
        
        
        
    }
    
    
    private var isBaseSet = false;
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        self.selected = true;
        clickStartAnimation(self)
        
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        self.selected = false;
        clickEndAnimation(self)


    }
    
    
    func startAnimation(timer timer : PomoTimer, second : Int, deltaTime : CGFloat , breakSecond : Int, outOf : Int) {
        let startAnglePosition = CGFloat(270 + 90);
        
        
        let secondToUse : CGFloat = timer.isOnBreak ? CGFloat(breakSecond) : CGFloat(second )
        breakStarted = timer.isOnBreak
        longBreakStarted = timer.isOnLongBreak
        let deltaFloat =  -deltaTime / 60 + secondToUse;
        
        currentPosition = CGFloat(deltaFloat )/CGFloat(outOf) * (startAnglePosition) // must do CGFloat division in order to produce the translation
        
        // print("current position : \(currentPosition)");
        
        // print("angle: \n\(progressAngle)  out of")
        
        
        self.progressAngle = currentPosition - 270
        
    }
    
    func resetAnim() {
        let resetMarker : CGFloat = -270
        let currentPosition = self.progressAngle
        
        
        
        //animate, button until it reaches the reset marker.
        // invalidate the timer when that occurs
        
        // print("currentPosition: \(currentPosition) ");
        
        
        self.progressAngle = currentPosition + markedPosition--;
        if(self.progressAngle <= resetMarker) {
            deltaTime?.invalidate();
            markedPosition = 0;
        }
        
    }
    
    func continueAnim() {
       // var resetMarker = self.progressAngle
        let currentPosition : CGFloat = -270
        
        // print("resetMarker : \(resetMarker)");
        
        self.progressAngle = currentPosition + markedPosition++ + markedPosition++;           //exponentially increases the speed
        if(self.progressAngle > self.currentPosition - 270) {
            deltaTime?.invalidate()
            
            // casting the type Any? as a function reference due to swift preventing us from making a function reference with an initializer
            
            let completionHandler = completion
            completionHandler!();
            
            markedPosition = 0;
        }
    }
    
    
    
    func resetAnimation(selector : Selector) {
        deltaTime = NSTimer.scheduledTimerWithTimeInterval(1/120, target: self, selector: selector, userInfo: nil, repeats: true)
        NSRunLoop.mainRunLoop().addTimer(deltaTime!, forMode: NSRunLoopCommonModes)
        deltaTime?.fire();
    }
    
    func animationWithHandler(selector : Selector, completion: () ->()) {
        deltaTime = NSTimer.scheduledTimerWithTimeInterval(1/120, target: self, selector: selector, userInfo: nil, repeats: true)
        NSRunLoop.mainRunLoop().addTimer(deltaTime!, forMode: NSRunLoopCommonModes)
        
        self.completion = completion
        deltaTime?.fire();
        
    }
    
    
    func clickStartAnimation(button : UIButton) {
        
        let vertOffset = CGAffineTransformMakeTranslation(0 , -6);
        UIView.animateWithDuration(0.4, animations: {
            button.transform =  vertOffset
            
            
            }, completion: nil)
    }
    
    func clickEndAnimation(button : UIButton) {
        UIView.animateWithDuration(0.4, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 9, options: [], animations: {
            button.transform = CGAffineTransformIdentity;
            
            
            }, completion: nil)
    }
    
    
    
    func resetStrokePosition() {
        self.currentPosition = 360
        self.markedPosition = 0;
    }
    
    func resetButton() {
        self.progressAngle = -270
        
    }
    
    func resetStatus() {
        self.longBreakStarted = false
        self.breakStarted = false;
    }
}

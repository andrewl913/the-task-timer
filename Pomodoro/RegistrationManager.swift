//
//  RegistrationManager.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 7/3/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit
import Parse

class RegistrationManager: NSObject, UITextFieldDelegate {
    // the purpose of this class is to handle new user registration
    
    var newUser : PFUser?
    var signUpController : SignUpViewController {
        didSet {
            // assign the delegate when we assign the signup controllerr
            self.signUpController.passwordTextField.delegate = self;
        }
    }
    
    //************ INITIALIZERS ****************
    init(signUpController : SignUpViewController) {
        
        self.signUpController = signUpController;
        self.signUpController.registerButton.isActive = false;
        
    }
    
    //****** PUBLIC INTERFACE *******************
    func registerUser() -> (user: PFUser, message: String) {
        var invalidAlertController : UIAlertController?
        
        var message : NSString = "Success";
        newUser = PFUser();
        
        newUser!.username = self.signUpController.usernameTextField.text
        newUser!.password = self.signUpController.passwordTextField.text;
        
        let isValidPass = isValidPassword(self.signUpController.passwordTextField.text!)
        
        if newUser!.username!.isEmpty {
            self.signUpController.usernameTextField.text = "";
            self.signUpController.usernameTextField.placeholder = "Can't be blank!"
            message = "error"
            return (PFUser(), message as String)
        }
        
        if newUser!.password!.isEmpty || (!isValidPass) {
            self.signUpController.passwordTextField.placeholder = "Can't be blank!"
            
            if(!isValidPass) {
                self.signUpController.passwordTextField.text = "";
            
                invalidAlertController = UIAlertController(title: "Invalid Password", message: "Must include 1 digit, 8 characters, 1 uppercase and 1 special character i.e \"!@#$%\" ", preferredStyle: UIAlertControllerStyle.ActionSheet)
                
                invalidAlertController?.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                self.signUpController.presentViewController(invalidAlertController!, animated: true, completion: nil)
            
                self.signUpController.passwordTextField.placeholder = "whoops..."
            }
            message = "error"
            return (PFUser(), message as String)
        }
        
        newUser?.signUpInBackgroundWithBlock {
            success, error in
            
            if let error = error {
                message = (error.userInfo["Something Went Wrong"] as? NSString)!
            } else {
                //successful registration
                
                
                self.signUpController.signInManager!.login()      //note.. caused an error before
            }
        }
        
        return (newUser!, message as String);
    }
    
    
    
    
    //****** PASSWORD TEXTFIELD DELEGATE ****************
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if ( range.length + range.location > textField.text!.characters.count) {
            return false;
        }
        // the username and password should be at max 15 characters.
        let newLength = textField.text!.characters.count + string.characters.count - range.length
        return newLength <= 15
    }
    
    
    func textFieldDidBeginEditing(textField: UITextField) {
        NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { queue in
            let userQuery = PFUser.query()
            
            userQuery?.whereKey("username", equalTo: self.signUpController.usernameTextField.text!)
            
            userQuery?.findObjectsInBackgroundWithBlock {
                (data : [AnyObject]? , error : NSError?) in
                
                if(error == nil ) {
                    print("found user", terminator: "")
                  
                    
                    if let objects = data as? [PFObject] {
                        //if the password is valid, change the text color to green
                        if (textField is PasswordTextField) {
                            if objects.count == 0 {
                                textField.textColor = self.isValidPassword(textField.text!) ?
                                    PomoUtils.startGreenColor : PomoUtils.tomatoRedColor
                                if ( self.signUpController.usernameTextField.text!.characters.count > 5 ) {
                                    self.signUpController.registerButton.isActive = true;
                                }
                            } else {
                                textField.textColor = UIColor.blackColor();
                            }
                        }//textfield
                    }//object check
                }//error check
            }//userquery
            
            if(textField is UsernameTextField) {
                //since we are entering the username again.. clear the password to avoid confusion
                self.signUpController.passwordTextField.text = ""
                self.signUpController.registerButton.isActive = false
                self.signUpController.passwordTextField.textColor = UIColor.blackColor()
            }
        } //notificationCenter
    }// end textfielddid
    
    
    
    // ***** HELPER METHODS **************
    
    private func isValidUsername(testStr: String) -> Bool {
        print("Validating username")
        let usernameRegEx = "^[a-z0-9_-]{3,15}$"
        
        let usernameTest = NSPredicate(format: "SELF MATCHES %@", usernameRegEx)
        return usernameTest.evaluateWithObject(testStr)
    }
    
    private func isValidPassword(testStr: String) -> Bool {
        print("Validating password");
        let passwordRegEx = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegEx);
        return  passwordTest.evaluateWithObject(testStr);
        
    }
    
    
}

//
//  SettingsViewController.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 5/17/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit
import Parse

protocol SetTimerDelegate {
    func setTime(time : Int) -> Int
    func setBreakTime(time : Int) -> Int
    func setLongBreakTime(time : Int) -> Int
}
protocol LogoutDelegate {
    func logoutClicked(logoutButton : UIButton);
}

class SettingsViewController: UITableViewController {
    
    @IBOutlet weak var timeDisplay: UILabel! // display to show the user before moving back to the timer
    @IBOutlet weak var breakDisplay: UILabel!
    @IBOutlet weak var longBreakDisplay: UILabel!
    
    @IBOutlet weak var longBreakSlider: UISlider!
    @IBOutlet weak var breakDurationSlider: UISlider!
    @IBOutlet weak var pomoDurationSlider: UISlider!
    var valueHasBeenSet = false;
    
    @IBOutlet weak var logoutButton: UIButton!
    
    var stopWarningController : StopWarningController?
    var mainViewController : MainViewController?
    var timerDelegate : SetTimerDelegate?
    var logoutDelegate : LogoutDelegate?
    
    @IBOutlet weak var timerSoundCell: UITableViewCell!
    
    @IBOutlet weak var signOutCell: UITableViewCell!
    
    var pomoMinute = 0,
    breakMinute = 0,
    longBreakMinute : Int = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self;
        
        
        clearHighlightedCells()
        
        timerSoundCell.separatorInset = UIEdgeInsetsZero
        signOutCell.separatorInset = UIEdgeInsetsZero
 
            
   
    }
    override func viewDidAppear(animated: Bool) {
        // set base values
        if(valueHasBeenSet) {
            
            pomoMinute = mainViewController!.pomoMinute
            breakMinute = mainViewController!.breakMinute
            longBreakMinute = mainViewController!.longBreakMinute
            
            setSliderValues(Float(pomoMinute), breakVal: Float(breakMinute), longBreakVal: Float(longBreakMinute))
            
            return;
        }
        setSliderValues(25, breakVal: 5, longBreakVal: 15)
       
        
        if self.mainViewController!.isSignedIn {
            logoutButton.setTitle("SIGN OUT", forState: .Normal)
        } else {
            logoutButton.setTitle("NOT SIGNED IN", forState: .Normal)
        
        }
 
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sliderHasBeenSet(sender: UISlider) {
        valueHasBeenSet = true;
        let timerValue = Int(58 * sender.value + 1);
        
       
        checkValue(timeDisplay, valueToCheck: timerValue)
        pomoMinute = self.timerDelegate!.setTime(timerValue)
    }
    
    
    @IBAction func breakSliderHasBeenSet(sender: UISlider) {
        valueHasBeenSet = true;
        let breakValue = Int(58 * sender.value + 1)
        
        checkValue(breakDisplay, valueToCheck: breakValue)
        breakMinute =  self.timerDelegate!.setBreakTime(breakValue)
    }
    
    @IBAction func longBreakSliderHasBeenSet(sender: UISlider) {
        valueHasBeenSet = true;
        let longBreakValue = Int(58 * sender.value + 1)
        checkValue(longBreakDisplay, valueToCheck: longBreakValue)
        longBreakMinute = self.timerDelegate!.setLongBreakTime(longBreakValue)
        
    }
    
    func checkValue(textDisplay : UILabel, valueToCheck : Int) {
        textDisplay.text = valueToCheck == 1 ? "\(valueToCheck) MINUTE" : "\(valueToCheck) MINUTES"
        
    }
    
    private func setSliderValues(pomoVal : Float , breakVal : Float, longBreakVal : Float) {
        breakDurationSlider.setValue(breakVal/58, animated: true);
        pomoDurationSlider.setValue(pomoVal/58, animated: true);
        longBreakSlider.setValue(longBreakVal/58, animated: true)
    }
    
    @IBAction func soundSwitchToggled(sender: UISwitch) {
       
        self.mainViewController!.timer!.avPlayer!.volume = sender.on ? 1.0 : 0.0;
        
        self.mainViewController?.timer?.canPlaySound = sender.on
        
        
        print(mainViewController!.timer!.avPlayer!.volume, terminator: "")
    }
    

    @IBAction func logOut(sender: UIButton) {
        logoutDelegate?.logoutClicked(sender)
        
        PFUser.logOutInBackgroundWithBlock({ error in
            if(error == nil) {
                self.mainViewController?.isSignedIn = false;
                self.mainViewController?.usernameLabel.text = "not signed in"
                if !self.mainViewController!.isSignedIn {
                    sender.setTitle("NOT SIGNED IN",forState: .Normal)
                } else {
                    sender.setTitle("successfully signed out..",forState: .Normal)
                }
                
                return;
            }
        })
    }
    
    func clearHighlightedCells() {
        for (var i = 0; i < self.tableView.numberOfRowsInSection(0) ; i++){
          let cell =  self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0))
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.None
        }
    }
    
    
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.row == 5) {
            if let cell = tableView.cellForRowAtIndexPath(indexPath) {
                cell.separatorInset = UIEdgeInsetsMake(0, CGRectGetWidth(self.view.bounds)/2.0, 0, CGRectGetWidth(self.view.bounds)/2.0)
            }
        }
    }

 
}

//
//  PomoNotificationListener.swift
//  Task Timer
//
//  Created by Andrew Lincoln on 7/10/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit
import Parse

class PomoNotificationListener: NSObject, PomoEventListenable {
    
    
    var notification : UILocalNotification = UILocalNotification();
    var longbreakStarted = false
    
    func setupNotification() {
        if longbreakStarted {
            notification.alertBody = "Long break started!"
        
            longbreakStarted = false;
        } else {
            notification.alertBody = "Break Started!"
            
        }
        notification.soundName = "Desk-bell-sound.wav"
        if let user = PFUser.currentUser() {
            notification.userInfo = ["userID" : user.objectId!]
        }
        
    }
 
    func start() {
        
    }
    
    func stop() {
     
    }
    
    func end() {
        
    }
    
    func timeIncremented() {
    
    }
    
    func breakStarted() {
        setupNotification();
        
        UIApplication.sharedApplication().presentLocalNotificationNow(notification)
        
    }
    
    func longBreakStarted() {
        longbreakStarted = true;
        setupNotification();
       
        UIApplication.sharedApplication().presentLocalNotificationNow(notification)
    }
    
    func taskAdded(outOf: Int) {
        
    }
    
    func taskFinished(outOf: Int) {
        
    }
    

}

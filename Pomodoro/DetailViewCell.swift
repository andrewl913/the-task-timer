//
//  DetailViewCell.swift
//  Task Timer
//
//  Created by Andrew Lincoln on 7/11/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class DetailViewCell: UITableViewCell {

    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var progressIntLabel: UILabel!
    @IBOutlet  var progressBar: ProgressBarView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    override func prepareForReuse() {
                
        removeProgressBar()
    }
    
    func removeProgressBar() {
        for view in self.contentView.subviews {
            if view is ProgressBarView {
                (view as! ProgressBarView).position = -200
            }
        }
    }
}

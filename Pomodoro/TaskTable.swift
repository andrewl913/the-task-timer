//
//  TaskTable.swift
//  Pomodoro
//
//  Created by Andrew Lincoln on 7/1/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class TaskTable: UITableView {
  
  var scrollView : UIScrollView?;
  //always push down the table cells to the bottom of it's view when any touches occur

  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    for view in self.subviews {
      if view is UIScrollView {
      self.scrollView = view as? UIScrollView
      }
    }

  }
}
